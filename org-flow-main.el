(defun ofm-go ()
  (interactive)
  (ofm-parse)
  (ofm-info ofm-flowChart)
  (message "place")
  (ofm-place)
  (ofm-info ofm-flowChart)
  (message "draw")
  (ofm-debug "flowchart BB = %s" (ofm-chart-BB))
  (ofm-draw)
  )
;;|--------------------------------------------------------------
;;|Description : get the content of a heading
;;|NOTE : inspired by https://hungyi.net/posts/org-mode-subtree-contents/
;;|-
;;|Author : jouke hylkema
;;|date   : 28-11-2023 11:11:43
;;|--------------------------------------------------------------
(defun ofm-get-subtree-contents ()
  "Get the content text of the subtree. Excludes the heading and any child subtrees."
  (interactive)
  (if (org-before-first-heading-p)
      (error "Not in or on an org heading")
    (save-excursion
      ;; If inside heading contents, move the point back to the heading
      ;; otherwise `org-agenda-get-some-entry-text' won't work.
      (unless (org-on-heading-p) (org-previous-visible-heading 1))
      (substring-no-properties
       (org-agenda-get-some-entry-text
        (point-marker)
        most-positive-fixnum))
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : parse the subhead for a flowchart
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 26-45-2023 12:45:14
;;|--------------------------------------------------------------
(defun ofm-parse (&optional silent)
  "parse the subhead for a flowchart"
  (interactive)
  (save-excursion
    (makunbound 'ofm-flowChart)
    (cl-loop until (string= (org-entry-get (point) "FLOWCHART") "start") do (outline-up-heading 1))
    (setq ofm-margin (string-to-number (org-entry-get (point) "TEXT_MARGIN"))
          ofm-group-margin (string-to-number (org-entry-get (point) "GROUP_MARGIN"))
          ofm-arrow-length (string-to-number (org-entry-get (point) "ARROW_LENGTH"))
          ofm-font-size (string-to-number (org-entry-get (point) "FONT_SIZE"))
          ofm-font-name (org-entry-get (point) "FONT_NAME")
          ofm-page-format (intern (format "%s_%s"
                                          (org-entry-get (point) "PAGE_SIZE")
                                          (org-entry-get (point) "PAGE_ORIENTATION")
                                          )
                                  )
          ofm-do-pages (org-entry-get (point) "PAGES")
          ofm-tmp-verbose (string-to-number (org-entry-get (point) "VERBOSE"))
          ofm-verbose (if silent 0 ofm-tmp-verbose)
          ofm-draw-groups (org-entry-get (point) "DRAW_GROUPS")
          ofm-shadows (org-entry-get (point) "SHADOWS")
          ofm-page-frame (org-entry-get (point) "FRAME")
          ofm-item-dict '()
          ofm-flowChart (ofm-flowchart)
          )
    )
  (setq ofm-verbose ofm-tmp-verbose)
  )
;;|--------------------------------------------------------------
;;|Description : position the elements
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 26-22-2023 12:22:26
;;|--------------------------------------------------------------
(defun ofm-place ()
  "position the elements"
  (interactive)
  (if (not (boundp 'ofm-flowChart))
      (display-warning :error "There is no flowchart defined !!")
    (cl-loop for item in (slot-value ofm-flowChart 'items) do
             (ofm-position (ofm-item-from-id item))
             )
    (ofm-debug "Fix overlaps")
    (cl-loop for index from 0
             for id1 in (slot-value ofm-flowChart 'items)
             for item1 = (ofm-item-from-id id1)
             do (ofm-group-overlap item1 index)
             )
    (let* ((BB (ofm-group-BB ofm-flowChart))
           (dx (+ ofm-margin (- (nth 0 BB))))
           (dy (+ ofm-margin (- (nth 1 BB))))
           )
      (ofm-translate-item ofm-flowChart (ofm-coords :x dx :y dy))
      (set-slot-value ofm-flowChart 'w (+ ofm-margin ofm-margin (nth 2 BB)))
      (set-slot-value ofm-flowChart 'h (+ 20 ofm-margin ofm-margin (nth 3 BB)))
      )
    (when ofm-do-pages
      (ofm-debug "Split pages")
      (cl-loop for id in (slot-value ofm-flowChart 'items)
               for item = (ofm-item-from-id id)
               do
               (let* ((w (nth 0 (plist-get ofm-page-dimensions ofm-page-format)))
                      (h (nth 1 (plist-get ofm-page-dimensions ofm-page-format)))
                      (ix (ofm-x item 'south))
                      (iy (ofm-y item 'south))
                      (pn (floor (/ iy h)))
                      (page (or (nth pn (slot-value ofm-flowChart 'pages))
                                (let ((page (ofm-page
                                             :size (nth 0 (string-split (format "%s" ofm-page-format) "_"))
                                             :orientation (nth 1 (string-split (format "%s" ofm-page-format) "_"))
                                             :n pn
                                             :path (format "%s_%s.svg" (slot-value ofm-flowChart 'path) pn)
                                             )
                                            )
                                      )
                                  (object-add-to-list ofm-flowChart 'pages page t)
                                  page
                                  )
                                )
                            )
                      )
                 (object-add-to-list page 'items item t)
                 )
               )
      ;; Add the page numbers
      (cl-loop for index from 0 to (- (length (slot-value ofm-flowChart 'pages)) 2)
               do
               (let* ((P1 (nth index (slot-value ofm-flowChart 'pages)))
                      (P2 (nth (+ index 1) (slot-value ofm-flowChart 'pages)))
                      (I1 (ofm-page-last P1))
                      (I2 (ofm-page-first P2))
                      (N1 (ofm-pageNumber :text (format "%s" (+ index 1))
                                          :from (slot-value I2 'from)
                                          :dir (slot-value I2 'dir)
                                          :id (org-id-new)
                                          )
                          )
                      (N2 (ofm-pageNumber :text (format "%s" index)
                                          :dir (slot-value I1 'dir)
                                          :id (org-id-new)
                                          )
                          )
                      
                      )
                 (ofm-chart-add N1)
                 (object-add-to-list P1 'items N1 t)
                 (ofm-position N1)

                 (set-slot-value N2 'from nil)
                 (ofm-move-item N2 'out (slot-value I2 'in) (ofm-coords :x 0 :y (- ofm-arrow-length)))
                 (set-slot-value I2 'from (slot-value N2 'id))
                 (object-add-to-list P2 'items N2)
                 (ofm-chart-add N2)
                 )
               )
      (cl-loop for p in (slot-value ofm-flowChart 'pages) do (ofm-page-position p))
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : draw the graph
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 26-55-2023 11:55:35
;;|--------------------------------------------------------------
(defun ofm-draw ()
  "draw the graph"
  (interactive)
  (if (not (boundp 'ofm-flowChart))
      (display-warning :error "There is no flowchart defined !!")
    (if ofm-do-pages
      (cl-loop for p in (slot-value ofm-flowChart 'pages)
               do
               (ofm-page-draw p)
               )
      (let* ((BB (ofm-group-BB ofm-flowChart))
             (x (- (nth 0 BB) 50))
             (y (- (nth 1 BB) 50))
             (w (+ 100 (nth 2 BB)))
             (h (+ 100 (nth 3 BB)))
             (path (format "%s.svg" (slot-value ofm-flowChart 'path)))
             (canvas (svg-create w h 
                                 :viewBox (format "%s %s %s %s" x y w h)
                                 :stroke-width 1
                                 :stroke "black"
                                 :font-family ofm-font-name
                                 :font-size ofm-font-size
                                 :alignment-baseline "middle"
                                 :dominant-baseline "middle"
                                 )
                     )
             )
        (ofm-chart-draw ofm-flowChart canvas path)
        )
      )
    (when ofm-draw-groups
      (cl-loop for (id . item) in ofm-item-dict do
               (when (string= "group" (slot-value item 'type))
                 (let* ((BB (ofm-group-BB item))
                        (x (- (nth 0 BB) 50))
                        (y (- (nth 1 BB) 50))
                        (w (+ 100 (nth 2 BB)))
                        (h (+ 100 (nth 3 BB)))
                        (path (format "%s_%s.svg"
                                      (slot-value ofm-flowChart 'path)
                                      (string-replace " " "_" (slot-value item 'text)))
                              )
                        (canvas (svg-create w h 
                                            :viewBox (format "%s %s %s %s" x y w h)
                                            :stroke-width 1
                                            :stroke "black"
                                            :font-family ofm-font-name
                                            :font-size ofm-font-size
                                            :alignment-baseline "middle"
                                            :dominant-baseline "middle"
                                            )
                                )
                        )
                   (ofm-draw-item item canvas path)
                   )
                 )
               )
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : BB of a list of items
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 30-46-2023 17:46:48
;;|--------------------------------------------------------------
(defun ofm-list-BB (items)
  "BB of a list of items"
  (interactive)
  (let* ((kids (remove nil items))
         (x1 (cl-loop for k in kids minimize (- (ofm-x k 'nw) (slot-value k 'dw))))
         (y1 (cl-loop for k in kids minimize (- (ofm-y k 'nw) (slot-value k 'dh))))
         (x2 (cl-loop for k in kids maximize (+ (ofm-x k 'se) (slot-value k 'dw))))
         (y2 (cl-loop for k in kids maximize (+ (ofm-y k 'se) (slot-value k 'dh))))
         (w (- x2 x1))
         (h (- y2 y1))
         )
      (list x1 y1 w h)
      )
    )
;;|--------------------------------------------------------------
;;|Description : get an item from its id
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 26-16-2023 13:16:52
;;|--------------------------------------------------------------
(defun ofm-item-from-id (id)
  "get an item from its id"
  (interactive "sId: ")
  (if (not (boundp 'ofm-item-dict))
      (display-warning :error "There is no flowchart defined !!")
    (cdr (assoc id ofm-item-dict))
    )
  )
;;|--------------------------------------------------------------
;;|Description : get an item based on a slot value
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 31-32-2023 10:32:49
;;|--------------------------------------------------------------
(defun ofm-item-from-slot (slot value)
  "get an item based on a slot value"
  (interactive)
  (cl-loop for (id . i) in ofm-item-dict
           if (and (slot-boundp i slot) (eq (slot-value i slot) value))
           return i)
  )
;;|--------------------------------------------------------------
;;|Description : get the BB of a string with a given font
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 09-26-2023 14:26:16
;;|--------------------------------------------------------------
(defun ofm-stringBB (string font size)
  "get the BB of a string with a given font"
  (interactive "P")
  (with-temp-buffer
    (face-remap-add-relative 'default `(:family ,font :height ,size))
    (insert string)
    (buffer-text-pixel-size)
    )
  )
;;|--------------------------------------------------------------
;;|Description : calculate the global BB
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 29-09-2023 12:09:35
;;|--------------------------------------------------------------
(defun ofm-chart-BB ()
  (let* ((kids (cl-loop for (id . k) in ofm-item-dict collect k)))
    (ofm-list-BB kids)
    )
  )
;;|--------------------------------------------------------------
;;|Description : Info if verbose > 0
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-54-2023 11:54:19
;;|--------------------------------------------------------------
(defun ofm-debug (fmt &rest txt)
  "Info if verbose > 0"
  (interactive "P")
  (if (> ofm-verbose 0) (apply 'message fmt txt))
  )
(provide 'org-flow-main)
