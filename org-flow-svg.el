;;|--------------------------------------------------------------
;;|Description : create a multiline text node
;;|NOTE : inspired from https://github.com/emacs-mirror/emacs/blob/master/lisp/svg.el
;;|-
;;|Author : jouke hylkema
;;|date   : 28-41-2023 13:41:27
;;|--------------------------------------------------------------
(defun ofm-svg-text (node text &rest args)
  "create a multiline text node"
  (interactive "P")
  (let* ((n1 (dom-node 'text `(,@(svg--arguments node args)))))
    (cl-loop for index from 0
             for l in text do
             (plist-put args :dy (format "%dem" index))
             (svg--append n1 (dom-node 'tspan
                                       `(,@(svg--arguments node args))
                                       (ofm-svg-handle-style (svg--encode-text l))
                                       )
                          )
             )
    (svg--append node n1)
    )
  )
;;|--------------------------------------------------------------
;;|Description : handle styles
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 31-49-2023 17:49:19
;;|--------------------------------------------------------------
(defun ofm-svg-handle-style (text)
  "handle styles"
  (interactive "P")
  (ofm-svg-handle-bold
   (ofm-svg-handle-italic
    (ofm-svg-handle-underline text)
    )
   )
  )
;;|--------------------------------------------------------------
;;|Description : replace *text* with <tspan font-weight="bold">text</tspan>
;;|NOTE : 
;;|- 
;;|Author : jouke hylkema
;;|date   : 31-58-2023 16:58:53
;;|--------------------------------------------------------------
(defun ofm-svg-handle-bold (text)
  "replace #text# with <tspan style>text</tspan>"
  (interactive "P")
  (if (string-match "\\(.*\\)\\(\\*\\)\\(.*\\)\\(\\*\\)\\(.*\\)" text)
      (let* ((S1 (match-string 1 text))
             (S2 (match-string 2 text))
             (S3 (match-string 3 text))
             (S4 (match-string 4 text))
             (S5 (match-string 5 text))
             )
        (format "%s<tspan font-weight='bold'>%s</tspan>%s" S1 S3 S5) 
        )
    text
    )
  )
;;|--------------------------------------------------------------
;;|Description : replace *text* with <tspan font-style='italic'>text</tspan>
;;|NOTE : 
;;|- 
;;|Author : jouke hylkema
;;|date   : 31-58-2023 16:58:53
;;|--------------------------------------------------------------
(defun ofm-svg-handle-italic (text)
  "replace #text# with <tspan style>text</tspan>"
  (interactive "P")
  (if (string-match "\\(.*\\)\\(/\\)\\(.+\\)\\(/\\)\\(.*\\)" text)
      (let* ((S1 (match-string 1 text))
             (S2 (match-string 2 text))
             (S3 (match-string 3 text))
             (S4 (match-string 4 text))
             (S5 (match-string 5 text))
             )
        ;; (message "replaced -%s- with -%s<tspan font-style='italic'>%s</tspan>%s-"
                 ;; text S1 S3 S5)
        (format "%s<tspan font-style='italic'>%s</tspan>%s" S1 S3 S5) 
        )
    text
    )
  )
;;|--------------------------------------------------------------
;;|Description : replace *text* with <tspan text-decoration='underline'>text</tspan>
;;|NOTE : 
;;|- 
;;|Author : jouke hylkema
;;|date   : 31-58-2023 16:58:53
;;|--------------------------------------------------------------
(defun ofm-svg-handle-underline (text)
  "replace #text# with <tspan style>text</tspan>"
  (interactive "P")
  (if (string-match "\\(.*\\)\\(__\\)\\(.*\\)\\(__\\)\\(.*\\)" text)
      (let* ((S1 (match-string 1 text))
             (S2 (match-string 2 text))
             (S3 (match-string 3 text))
             (S4 (match-string 4 text))
             (S5 (match-string 5 text))
             )
        (format "%s<tspan text-decoration='underline'>%s</tspan>%s" S1 S3 S5) 
        )
    text
    )
  )
;;|--------------------------------------------------------------
;;|Description : draw a list box
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 18-31-2023 11:31:19
;;|--------------------------------------------------------------
(defun ofm-svg-list (node text &rest args)
  "draw a list box"
  (interactive "P")
  (let* ((n1 (dom-node 'text `(,@(svg--arguments node args)))))
    (cl-loop for index from 0
             for l in text do
             (plist-put args :dy (format "%dem" index))
             (svg--append n1 (dom-node 'tspan
                                       `(,@(svg--arguments node args))
                                       (ofm-svg-handle-style
                                        (svg--encode-text (format "‣ %s" l))
                                        )
                                       )
                          )
             )
    (svg--append node n1)
    )
  )
;;|--------------------------------------------------------------
;;|Description : draw a numbered list box
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 18-31-2023 11:31:19
;;|--------------------------------------------------------------
(defun ofm-svg-nlist (node text &rest args)
  "draw a list box"
  (interactive "P")
  (let* ((n1 (dom-node 'text `(,@(svg--arguments node args)))))
    (cl-loop for index from 0
             for l in text do
             (plist-put args :dy (format "%dem" index))
             (svg--append n1 (dom-node 'tspan
                                       `(,@(svg--arguments node args))
                                       (ofm-svg-handle-style
                                        (svg--encode-text (format "%s. %s" (+ index 1) l))
                                        )
                                       )
                          )
             )
    (svg--append node n1)
    )
  )
;;|--------------------------------------------------------------
;;|Description : determine the dimension sof a string of text
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 06-04-2023 13:04:41
;;|--------------------------------------------------------------
(defun ofm-svg-textBB (text &optional scale margin)
  "determine the dimension of a string of text"
  (interactive)
  (with-temp-buffer
    (face-remap-add-relative 'default `(:family ,ofm-font-name :height ,(* 10 ofm-font-size)))
    (let* ((text (replace-regexp-in-string "\n$" "" text))
           (dubby (insert text))
           (BB (buffer-text-pixel-size))
           (w (+ (* 0.8 (car BB)) (or margin 0)))
           (h (+ (* 0.61 (cdr BB)) (or margin 0)))
           )
      (list w h)
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : draw a page number
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-18-2023 19:18:35
;;|--------------------------------------------------------------
(defun ofm-svg-pageNumber (canvas n w &optional x y)
  "draw a page number"
  (interactive "P")
  (let* ((x (or x (- w 12)))
         (y (or y 12))
         (r 10)
         (points `((,(- x r) . ,(- y r))
                   (,(+ x r) . ,(- y r))
                   (,(+ x r) . ,(+ y (- r 3)))
                   (,x . ,(+ y r))
                   (,(- x r) . ,(+ y (- r 3)))
                   (,(- x r) . ,(- y r))
                   )
                 )
         )
    (svg-polyline canvas points :fill "white" :stroke "black")
    
    (svg-text canvas (format "%s" n)
              :x x
              :y y
              :text-anchor "middle"
              )
    )
  )
;;|--------------------------------------------------------------
;;|Description : draw a frame
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 02-24-2023 15:24:38
;;|--------------------------------------------------------------
(defun ofm-svg-frame (canvas w h)
  "draw a frame"
  (svg-rectangle canvas 1 1 (- w 1) (- h 1)
                 :fill "none"
                 )
  )
;;|--------------------------------------------------------------
;;|Description : get the height and with of an svg file
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 18-01-2023 13:01:46
;;|--------------------------------------------------------------
(defun ofm-svg-fileBB (path)
  "get the height and with of an svg file"
  (interactive "P")
  (let* ((xml (car (xml-parse-file path )))
         (width (string-to-number (cdr (assq 'width (xml-node-attributes xml)))))
         (height (string-to-number (cdr (assq 'height (xml-node-attributes xml)))))
         )
    (list width height)
    )
  )
;;|--------------------------------------------------------------
;;|Description : get the BB of an svg object in a xml dom
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 29-57-2024 17:57:16
;;|--------------------------------------------------------------
(defun ofm-svg-xmlBB (xml)
  "get the B of an svg object in a xml dom"
  (interactive)
  (mapcar 'string-to-number (split-string (cdr (assq 'viewBox (xml-node-attributes xml)))))
  )
(provide 'org-flow-mode)
