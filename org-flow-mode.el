(define-derived-mode org-flow-mode org-mode "org-flow mode"
  "a variant of org-mode that produces SVG flowcharts"
  (setq debug-on-error t
        ofm-margin 0
        ofm-group-margin 0
        ofm-draw-groups nil
        ofm-arrow-length 0
        ofm-font-size 0
        ofm-font-name "FONT_NAME"
        ofm-verbose 0
        ofm-item-dict '()
        ofm-page-dimensions '(A4_Portrait (793 1122)
                              A4_Landscape (1122 793)
                              Latex_Portrait (453 708)            
                              Latex_Landscape (708 453)            
                              )
        ofm-item-types '(("rect"      . ofm-rect)
                         ("check"     . ofm-check)
                         ("decision"  . ofm-decision)
                         ("delay"     . ofm-delay)
                         ("list"      . ofm-list)
                         ("nlist"     . ofm-nlist)
                         ("group"     . ofm-group)
                         ("until"     . ofm-until)
                         ("while"     . ofm-while)
                         ("latex"     . ofm-latex)
                         ("connect"   . ofm-connect)
                         )
        ofm-item-inputs '()
        ofm-inputs '(("DIR" . (completing-read "DIR: " '("None" "TB" "BT" "LR" "RL")))
                     ("FROM" . (completing-read "FROM: " (ofm-get-candidates)))
                     ("TO" . (completing-read "TO: " (ofm-get-candidates)))
                     ("FROM_PORT" . (completing-read "FROM_PORT: " '("None" "north" "east" "south" "west")))
                     ("TO_PORT" . (completing-read "TO_PORT: " '("None" "north" "east" "south" "west")))
                     ("BACKGROUND" . (read-string "background color: " "white"))
                     ("D" . (read-string "D: " "0"))
                     ("SEUL" . (completing-read "SEUL: " '(t nil)))
                     ("INVERSE" . (completing-read "INVERSE: " '(t nil)))
                     )
        ofm-shadows nil
        ofm-page-frame nil
        ofm-do-pages nil
        )
  ;; === Menu ===
  (easy-menu-define org-menu org-mode-map "Org flow menu"
    `("Org flow"
      ,(append (list "Insert" ["Flowchart" (ofm-insert-flowchart) t])
              (mapcar (lambda (item)
                        (let ((key (car item)))
                          `[,key (ofm-add-item ,key) t]
                          )
                        )
                      ofm-item-types
                      )
              )
      ("Actions"
       ["Draw" (ofm-go) t]
       )
      )
    )
  (let* ((root (file-name-directory (locate-file "org-flow-mode.el" load-path))))
    :after-hook (load-file (format "%sorg-flow-main.el" root))
    :after-hook (load-file (format "%sorg-flow-ui.el" root))
    :after-hook (load-file (format "%sorg-flow-svg.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-coords.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-anchors.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-page.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-item.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-rectangle.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-decision.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-group.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-chart.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-until.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-check.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-list.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-nlist.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-delay.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-pageNumber.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-latex.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-while.el" root))
    :after-hook (load-file (format "%sClasses/org-flow-connect.el" root))
    )
  )

(provide 'org-flow-mode)
