;;|--------------------------------------------------------------
;;|Description : dummy
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 25-33-2023 16:33:46
;;|--------------------------------------------------------------
(defun ofm-insert-flowchart ()
  "insert a flowchart"
  (interactive "stitle: \nfoutput file name:")
  (let* ((title (read-string "Title: "))
         (path (read-file-name "Output file: "))
         (ext (file-name-extension path))
         (text (read-string "Text: "))
         )
    (org-insert-heading-after-current)
    (insert title)
    (org-entry-put (point) "PATH" path)
    (org-entry-put (point) "FORMAT" (file-name-extension path))
    (org-entry-put (point) "TEXT_MARGIN" "10")
    (org-entry-put (point) "GROUP_MARGIN" "8")
    (org-entry-put (point) "FONT_SIZE" "10")
    (org-entry-put (point) "ARROW_LENGTH" "28")
    (org-entry-put (point) "FONT_NAME" "DejaVu Sans")
    (org-entry-put (point) "FLOWCHART" "start")
    (org-entry-put (point) "VERBOSE" "0")
    (org-entry-put (point) "PAGES" "nil")
    (org-entry-put (point) "PAGE_SIZE" "A4")
    (org-entry-put (point) "PAGE_ORIENTATION" "Portrait")
    (org-entry-put (point) "DRAW_GROUPS" "nil")
    (org-entry-put (point) "SHADOWS"  "t")
    (org-entry-put (point) "FRAME" "nil")
    (org-end-of-meta-data t)
    (insert text)
    (newline)
    (ofm-parse)
    )
  )
;;|--------------------------------------------------------------
;;|Description : insert an item
;;|NOTE : try to define the data in the class itself
;;|-
;;|Author : jouke hylkema
;;|date   : 04-29-2024 10:29:37
;;|--------------------------------------------------------------
(defun ofm-add-item (name)
  "insert an item"
  (interactive (list (completing-read "type: " ofm-item-types)))
  (let* ((id (org-id-new))
         (text (read-string "Text: "))
         )
    (org-insert-heading-respect-content)
    (insert (format "%s %s" name text))
    (newline)
    (insert text)
    (org-entry-put (point) "ID" id)
    (org-entry-put (point) "DISABLED" "nil")
    (cl-loop for i in (cdr (assoc name ofm-item-inputs))
             for key = i
             for val = (eval (cdr (assoc i ofm-inputs)))
             if (string= key "TO") do (org-entry-put (point) key (nth 2 (string-split val ":@:"))) 
             else if (string= key "FROM") do (org-entry-put (point) key (nth 2 (string-split val ":@:")))
             else unless (string= val "None") do (org-entry-put (point) key val)
             )
    )
  )
;;|--------------------------------------------------------------
;;|Description : insert an item between two others
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 26-44-2023 13:44:06
;;|--------------------------------------------------------------
(defun ofm-insert-item (name)
  "insert an item between two others"
  (interactive (list (completing-read "type: " ofm-item-types)))
  (ofm-parse t)
  (let* ((top (org-entry-get (point) "ID"))
         (bottom (ofm-chart-find-next top))
         (newId (ofm-add-item name))
         )
    (if (not top) (error "Can't find the element above insertion")
      (save-excursion
        (org-id-goto bottom)
        (org-entry-put (point) "FROM" newId)
        )
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : remove an item
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 26-52-2023 14:52:29
;;|--------------------------------------------------------------
(defun ofm-remove-item ()
  "remove an item"
  (interactive)
  (let* ((actual (org-entry-get (point) "ID"))
         (previous (org-entry-get (point) "FROM"))
         (next (ofm-chart-find-next actual))
         )
    (org-cut-subtree)
    (when (and previous next)
      (org-id-goto next)
      (org-entry-put (point) "FROM" previous)
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : insert a group
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 01-11-2023 17:11:11
;;|--------------------------------------------------------------
(defun ofm-insert-group (name)
  "insert a group"
  (org-insert-heading-after-current)
  (insert name)
  (org-entry-put (point) "ID" (org-id-new))
  )
;;|--------------------------------------------------------------
;;|Description : get a list of all element ids and first 10 characters from text
;;|NOTE : only elements that have a 'slot property are selected
;;|-
;;|Author : jouke hylkema
;;|date   : 01-40-2023 14:40:30
;;|--------------------------------------------------------------
(defun ofm-get-candidates ()
  "get a list of all element ids and first 10 characters from text"
  (interactive)
  (ofm-parse t)
  (cl-loop for (id . c) in (nreverse ofm-item-dict)
           for type = (slot-value c 'baseType)
           for text = (format "%s %s ..."
                              (nth 0 (split-string (slot-value c 'text)))
                              (nth 1 (split-string (slot-value c 'text)))
                              )
           ;; unless (or (string= "group" type) (string= "until" type))
           unless (string= type "group")
           collect (format "%s:@:%s:@:%s" type text id) into out
           finally return (push "None" out)
           )
  )
(provide 'org-flow-ui)
