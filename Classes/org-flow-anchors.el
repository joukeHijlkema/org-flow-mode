;;|--------------------------------------------------------------
;;|Description : The anchors of an item
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 08-14-2024 14:14:03
;;|--------------------------------------------------------------
(defclass ofm-anchors ()
  ((baseType :initarg baseType :initform "anchor" :type string)
   (north  :initarg :north  :initform (ofm-coords :x 0 :y 0)  :type ofm-coords)
   (east   :initarg :east   :initform (ofm-coords :x 0 :y 0)  :type ofm-coords)
   (south  :initarg :south  :initform (ofm-coords :x 0 :y 0)  :type ofm-coords)
   (west   :initarg :west   :initform (ofm-coords :x 0 :y 0)  :type ofm-coords)
   (ne     :initarg :ne     :initform (ofm-coords :x 0 :y 0)  :type ofm-coords)
   (se     :initarg :se     :initform (ofm-coords :x 0 :y 0)  :type ofm-coords)
   (sw     :initarg :sw     :initform (ofm-coords :x 0 :y 0)  :type ofm-coords)
   (nw     :initarg :nw     :initform (ofm-coords :x 0 :y 0)  :type ofm-coords)
   (center :initarg :center :initform (ofm-coords :x 0 :y 0)  :type ofm-coords)
   (in     :initarg :in     :initform (ofm-coords :x 0 :y 0) :type ofm-coords)
   (out    :initarg :out    :initform (ofm-coords :x 0 :y 0) :type ofm-coords)xs
   )
  "The anchors of an item"
  )
;;|--------------------------------------------------------------
;;|Description : return an info string (indented)
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 08-43-2024 17:43:39
;;|--------------------------------------------------------------
(cl-defmethod ofm-anchors-info ((item ofm-anchors) )
  (format "\n - center: %s\n - in: %s\n - out: %s"
          (ofm-coords-print (slot-value item 'center))
          (ofm-coords-print (slot-value item 'in))   
          (ofm-coords-print (slot-value item 'out))  
          )
  )
;;|--------------------------------------------------------------
;;|Description : Calculate anchors based n w and h
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 08-16-2024 12:16:58
;;|--------------------------------------------------------------
(cl-defmethod ofm-calc-anchors ((item ofm-anchors) parent)
  (let ((w (slot-value parent 'w))
        (h (slot-value parent 'h))
        (dir (slot-value parent 'dir))
        (type (slot-value parent 'baseType))
        (from (ofm-from parent)))
        
    (set-slot-value item 'center (ofm-coords :x (* 0.5 w) :y (* 0.5 h)))
    (set-slot-value item 'north  (ofm-coords :x (* 0.5 w) :y 0        ))
    (set-slot-value item 'east   (ofm-coords :x w         :y (* 0.5 h)))
    (set-slot-value item 'south  (ofm-coords :x (* 0.5 w) :y h        ))
    (set-slot-value item 'west   (ofm-coords :x 0         :y (* 0.5 h)))
    (set-slot-value item 'ne     (ofm-coords :x w         :y 0        ))
    (set-slot-value item 'se     (ofm-coords :x w         :y h        ))
    (set-slot-value item 'sw     (ofm-coords :x 0         :y h        ))
    (set-slot-value item 'nw     (ofm-coords :x 0         :y 0        ))
    (when (string= "item" type)
      (set-slot-value
       item
       'out
       (cond ((string= dir "LR") (slot-value item 'east))
             ((string= dir "RL") (slot-value item 'west))
             ((string= dir "TB") (slot-value item 'south))
             ((string= dir "BT") (slot-value item 'north)))
       )
      (set-slot-value
       item
       'in
       (cond ((string= dir "LR") (slot-value item 'west))
             ((string= dir "RL") (slot-value item 'east))
             ((string= dir "TB") (slot-value item 'north))
             ((string= dir "BT") (slot-value item 'south)))
       )
      (when from
        (set-slot-value
         (slot-value from 'anchors)
         'out
         (cond ((string= dir "LR") (ofm-a from 'east))
               ((string= dir "RL") (ofm-a from 'west))
               ((string= dir "TB") (ofm-a from 'south))
               ((string= dir "BT") (ofm-a from 'north)))
         )
        )
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : translate the anchors
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 08-20-2024 13:20:09
;;|--------------------------------------------------------------
(cl-defmethod ofm-translate-anchors ((item ofm-anchors) C)
  (ofm-coord-plus (slot-value item 'north)  C 1)
  (ofm-coord-plus (slot-value item 'east)   C 1)
  (ofm-coord-plus (slot-value item 'south)  C 1)
  (ofm-coord-plus (slot-value item 'west)   C 1)
  (ofm-coord-plus (slot-value item 'ne)     C 1)
  (ofm-coord-plus (slot-value item 'se)     C 1)
  (ofm-coord-plus (slot-value item 'sw)     C 1)
  (ofm-coord-plus (slot-value item 'nw)     C 1)
  (ofm-coord-plus (slot-value item 'center) C 1)
  )
;;|--------------------------------------------------------------
;;|Description : move the anchors
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 09-50-2024 15:50:27
;;|--------------------------------------------------------------
(cl-defmethod ofm-move-anchors ((item ofm-anchors) C)
  (ofm-translate-anchors item (ofm-coord-min C (slot-value item 'center) 1 t))
  )
