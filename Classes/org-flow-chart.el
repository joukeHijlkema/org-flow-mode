;;|--------------------------------------------------------------
;;|Description : flowchart
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 25-48-2023 16:48:08
;;|--------------------------------------------------------------
(defclass ofm-flowchart (ofm-group)
  ((title :initarg :title
         :initform "flowchart"
         :type string)
   (path :initarg :path
         :initform ""
         :type string
         :documentation "the path of the result file")
   (format :initarg :format
           :initform "svg"
           :documentation "the format of the result file")
   (pages :initarg :pages
          :initform '()
          :documentation "the pages of the document (one file per page)")
   )
  "a flowchart class"
  )
;;|--------------------------------------------------------------
;;|Description : initialise chart
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 13-39-2023 11:39:45
;;|--------------------------------------------------------------
(cl-defmethod initialize-instance :after ((item ofm-flowchart) &rest _)
  (ofm-debug "flowchart constructor")
  (let* ((title (org-get-heading))
         (path (org-entry-get (point) "PATH"))
         (format (org-entry-get (point) "FORMAT"))
         )
    (set-slot-value item 'title title)
    (set-slot-value item 'path path)
    (set-slot-value item 'format format)
    (set-slot-value item 'type "flowchart")
    )
  )
;;|--------------------------------------------------------------
;;|Description : chart info
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 13-21-2023 11:21:42
;;|--------------------------------------------------------------
(cl-defmethod ofm-info ((flow ofm-flowchart) &optional text)
  (when (> ofm-verbose 0)
    (cl-loop for (id . item) in ofm-item-dict do
             (message "%s %s" (make-string (slot-value item 'level) ?-) id)
             )
    (cl-loop for id in (slot-value flow 'items)
             for item = (ofm-item-from-id id)
             do (message "flowchart Toplevel Item %s (%s)" id (ofm-coords-print (ofm-a item 'center)))
             )
    (message "Pages: %s" (length (slot-value flow 'pages)))
    )
  (when  (> ofm-verbose 1)
    (let ((slots (list 'id
                       'type
                       'path
                       'format
                       )
                 )
          )
      (cl-call-next-method flow text slots)
      (cl-loop for (id . item) in ofm-item-dict do
               (if id (ofm-info item "flowchart contains: "))
               )
      (cl-loop for id in (slot-value flow 'items) do (message "flowchart Toplevel Item %s" id))
     )
    )
  )
;|--------------------------------------------------------------
;;|Description : translate all items in dict
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 30-59-2023 17:59:44
;;|--------------------------------------------------------------
(cl-defmethod ofm-group-translate ((item ofm-flowchart) Delta &optional start)
  (let ((start (or start 0))
        )
    (ofm-debug "start = %s" start)
    (cl-loop for i from start
             for target = (ofm-item-from-id (nth i (slot-value item 'items)))
             while target
             do
             (ofm-debug "translate %s with %s" (slot-value target 'id) Delta)
             (ofm-translate-item target Delta)
             )
    )
  )
 ;;|--------------------------------------------------------------
;;|Description : add an item to the overall dict
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 24-26-2023 11:26:23
;;|--------------------------------------------------------------
(defun ofm-chart-add (item)
  "add an item to the overall dict"
  (interactive "P")
  (let ((id (slot-value item 'id)))
    (setq ofm-item-dict (append (remove nil ofm-item-dict) `((,id . ,item)))) ;;To keep track of everything
    )
  )
;;|--------------------------------------------------------------
;;|Description : find the next item of targetId
;;|NOTE : thuis is the item that has targetId as 'from
;;|-
;;|Author : jouke hylkema
;;|date   : 26-49-2023 13:49:10
;;|--------------------------------------------------------------
(defun ofm-chart-find-next (targetId)
  "find the origin given the from item"
  (interactive "P")
  (cl-loop for (id . item) in ofm-item-dict
           until (string= targetId (slot-value item 'from))
           finally return id
           )
  )
;;|--------------------------------------------------------------
;;|Description : draw the flow chart
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 09-06-2024 15:06:53
;;|--------------------------------------------------------------
(cl-defmethod ofm-chart-draw ((item ofm-flowchart) canvas path)
  (ofm-debug "draw chart")
  (cl-loop for i in (slot-value item 'items) do
           (ofm-draw-item (ofm-item-from-id i) canvas)
           )
    (with-temp-file path
      (set-buffer-multibyte nil)
      (insert "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"no\"?>")
      (svg-print canvas)
      )
    )
