;;|--------------------------------------------------------------
;;|Description : a check box
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 06-10-2023 16:10:23
;;|--------------------------------------------------------------
(push '("check" . ("DIR" "FROM" "FROM_PORT")) ofm-item-inputs)
(defclass ofm-check (ofm-item)
  ()
  "a check box"
  )
(cl-defmethod initialize-instance :after ((item ofm-check) &rest _)
  (ofm-debug  "checkbox constructor id=%s" (slot-value item 'id))
  (set-slot-value item 'type "check")
  (set-slot-value item 'w (+ (slot-value item 'h) (slot-value item 'w)))
  (ofm-create-anchors item)
  )
;;|--------------------------------------------------------------
;;|Description : Parse a checkbox
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 06-30-2023 16:30:10
;;|--------------------------------------------------------------
(cl-defmethod ofm-check-parse (target)
  "parse the item"
  (ofm-debug  "check block")
  (let* ((txt (ofm-get-subtree-contents))
         (id (org-entry-get (point) "ID"))
         (from (org-entry-get (point) "FROM"))
         (dir (org-entry-get (point) "DIR"))
         (bg (org-entry-get (point) "BACKGROUND"))
         (lar (string-to-number 
               (format "%s"
                       (or (org-entry-get (point) "ARROW_LENGTH") ofm-arrow-length)
                       )
               )
              ) ;; it is either a string or a number ...
         (item (ofm-check
                :text txt
                :id id
                :dir dir
                :type "check"
                )
               )
         )
    (when from (set-slot-value item 'from from))
    (when bg (set-slot-value item 'bg bg))
    (when lar (set-slot-value item 'lar lar))
    item
   )
  )
;;|--------------------------------------------------------------
;;|Description : info
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 10-17-2023 17:17:08
;;|--------------------------------------------------------------
(cl-defmethod ofm-info ((item ofm-check) &optional text force)
  (let ((slots (list 'id
                     'type
                     'in
                     'out
                     'anchors
                     'w
                     'h
                     'text
                     'from
                     `dir
                     'bg
                     )
               )
        )
    (cl-call-next-method item text slots force)
    )
  )
;;|--------------------------------------------------------------
;;|Description : draw the chackbox item
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 06-13-2023 16:13:18
;;|--------------------------------------------------------------
(cl-defmethod ofm-draw-item ((item ofm-check) canvas)
  "draw the chackbox item"
  (ofm-info item "Draw checkbox: " )
  (let* ((g (svg-node canvas 'g))
         (x (ofm-x item))
         (y (ofm-y item))
         (w (slot-value item 'w))
         (h (slot-value item 'h))
         (h2 (* 0.8 h))
         (x2 (+ x (* 0.1 h2)))
         (y2 (+ y (* 0.1 h)))
         (xc (+ x (* 0.5 (+ w h))))
         (nl (length (slot-value item 'lines)))
         (D  (/ h nl))
         (yc (+ y (* 0.5 D)))
         )
    (ofm-item-draw-shade item g)
    (svg-rectangle g x y w h
                   :fill-opacity 0.5
                   :fill (slot-value item 'bg)
                   )
    (svg-rectangle g x2 y2 h2 h2
                   :fill "white"
                   :stroke "black"
                   )
    (ofm-svg-text g (slot-value item 'lines)
                  :x xc
                  :y yc
                  :text-anchor "middle"
                  :alignment-baseline "middle"
                  :fill "black"
                  :stroke "none"
                  )
    (ofm-draw-connection item canvas)
    ) 
  ;; DEBUG
  (ofm-drawInOut item canvas)
  ;; END DEBUG
  )
