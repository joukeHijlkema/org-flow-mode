;;|--------------------------------------------------------------
;;|Description : a group class
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 29-43-2023 11:43:59
;;|--------------------------------------------------------------
(defclass ofm-group (ofm-item)
  ((items :initarg items
          :initform '()
          :type list)
   (baseType :initarg baseType
             :initform "group"
             :type string)
   (place :initarg place
          :initform nil
          :type list)
   )
"a group class"
)
(cl-defmethod initialize-instance :after ((item ofm-group) &rest _)
  (ofm-debug "group constructor")
  (set-slot-value item 'type "group")
  (when (org-entry-get (point) "PLACE")
    (set-slot-value item 'place (split-string (org-entry-get (point) "PLACE"))))
  (let ((level (format "LEVEL=%s" (+ 1 (org-current-level)))))
    (org-map-entries
     '(lambda ()
        (unless (org-entry-get (point) "DISABLED")
          (let ((type (nth 0 (string-split (org-get-heading))))
                (id (org-entry-get (point) "ID"))
                )
            (ofm-debug "\n=== PARSE %s %s ===" type id)
            (if (assoc type ofm-item-types)
                (ofm-addItem item (funcall (cdr (assoc type ofm-item-types))) t)
              (error "I don't know what to do with %s" type))
            (ofm-debug "=== END PARSE %s %s ===" type id)
            )
          )
        )
     level 'tree
     )
    )
  )
;;|--------------------------------------------------------------
;;|Description : info
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 10-17-2023 17:17:08
;;|--------------------------------------------------------------
(cl-defmethod ofm-info ((item ofm-group) &optional text force)
  (let ((slots (list 'id
                     'type
                     'anchors
                     'w
                     'h
                     'dw
                     'dh
                     'text
                     'items
                     'in
                     'out
                     )
               )
        )
    (cl-call-next-method item text slots force)
    )
  )
;;|--------------------------------------------------------------
;;|Description : add an item id to the ITEMS
;;|NOTE : the correspondance id->item is stored in the ofm-item-dict dict
;;|-
;;|Author : jouke hylkema
;;|date   : 29-09-2023 17:09:18
;;|--------------------------------------------------------------
(cl-defmethod ofm-addItem ((group ofm-group) item &optional rear)
  (let ((id (slot-value item 'id)))
   (ofm-debug "add %s to %s (rear = %s)" id (slot-value group 'id) rear)
   (object-add-to-list group 'items id rear)
   (ofm-chart-add item)
   )
 )
;;|--------------------------------------------------------------
;;|Description : calculate the global BB
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 29-09-2023 12:09:35
;;|--------------------------------------------------------------
(cl-defmethod ofm-group-BB ((item ofm-group))
  "get the BB of a list of items"
  (let* ((kids (cl-loop for k in (slot-value item 'items) collect (ofm-item-from-id k))))
    (ofm-list-BB kids)
    )
  )
;;|--------------------------------------------------------------
;;|Description : draw a group
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 29-05-2023 12:05:51
;;|--------------------------------------------------------------
(cl-defmethod ofm-draw-item ((item ofm-group) canvas &optional path)
  (ofm-debug "Draw group %s" (slot-value item 'text))
  (let* ((w (slot-value item 'w))
         (h (slot-value item 'h))
         (x (ofm-x item))
         (y (ofm-y item))
         (title (nth 0 (slot-value item 'lines)))
         (bBB (ofm-svg-textBB title 0.5 5))
         (wb (nth 0 bBB))
         (hb (nth 1 bBB))
         (xb (+ x 2))
         (yb (- y (* 0.5 hb)))
         (xt (+ xb (* 0.5 wb)))
         (yt (+ yb (* 0.5 hb)))
         (g (svg-node canvas 'g))
         )
    (unless path
      (ofm-item-draw-shade item g)
      (svg-rectangle g x y w h
                     :fill-opacity 0.5
                     :stroke-width 0.5
                     :fill (slot-value item 'bg)
                     )
      (svg-rectangle g xb yb wb hb
                     :fill "white"
                     :stroke-width 0.5
                     )
      (svg-text g title
                :x xt
                :y yt
                :dominant-baseline "middle"
                :text-anchor "middle"
                :fill "black"
                :stroke "none")
      )
    (cl-loop for i in (slot-value item 'items) do (ofm-draw-item (ofm-item-from-id i) g))
    (when path
      (with-temp-file path
        (set-buffer-multibyte nil)
        (insert "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"no\"?>")
        (svg-print canvas)
        )
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : position the item relative from FROM
;;|NOTE : Not complete, I need to implement the anchor class to realy use the PLACE key
;;|-
;;|Author : jouke hylkema
;;|date   : 29-56-2023 14:56:48
;;|--------------------------------------------------------------
(cl-defmethod ofm-position ((item ofm-group))
  (ofm-debug "\n Group position %s (%s)" (slot-value item 'text) (slot-value item 'type))
  (cl-loop for i in (slot-value item 'items) do (ofm-position (ofm-item-from-id i)))
  (ofm-debug "Fix conectors")
  (cl-loop for (id . item) in ofm-item-dict
           if (string= "arrow" (slot-value item 'type)) do (ofm-connect-update item))
  (let* ((BB (ofm-group-BB item))
         (offset (ofm-coords
                  :x (- (nth 0 BB) ofm-group-margin)
                  :y (- (nth 1 BB) ofm-group-margin)
                  )
                 )
         (width (+ ofm-group-margin ofm-group-margin (nth 2 BB)))
         (height (+ ofm-group-margin ofm-group-margin (nth 3 BB)))
         (center (ofm-coords :x (* 0.5 width) :y (* 0.5 height)))
         )
    (set-slot-value item 'w width)
    (set-slot-value item 'h height)
    (ofm-create-anchors item)
    (ofm-translate-item item offset t)
    )
  (when (slot-value item 'place)
    (let* ((where (nth 0 (slot-value item' place)))
           (parent (nth 1 (slot-value item 'place)))
           (A1 (intern (nth 2 (slot-value item 'place))))
           (A2 (intern (nth 3 (slot-value item 'place))))
           (N (cond ((string= where "below") (ofm-coords :x 0  :y 30))
                    ((string= where "above") (ofm-coords :x 0  :y -30))
                    ((string= where "left")  (ofm-coords :x -30 :y 0))
                    ((string= where "right") (ofm-coords :x 30  :y 0))))
           (P1 (ofm-a (ofm-item-from-id parent) A1))
           (P2 (ofm-a item A2))
           (C (ofm-coord-plus N (ofm-coord-min P1 P2 1 t) 1 t))
           )
      (ofm-move-item item 'north P1 N)
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : measure the overlap of 2 items
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 18-14-2023 16:14:32
;;|--------------------------------------------------------------
(cl-defmethod ofm-group-overlap ((item ofm-item) stop)
  (cl-loop  for index from 0 to (- stop 1)
            for item2 = (ofm-item-from-id (nth index (slot-value ofm-flowChart 'items)))
            do (ofm-fix-overlap item item2 stop)
            )
  )
;;|--------------------------------------------------------------
;;|Description : fix an eventual ovelap
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 18-56-2023 17:56:41
;;|--------------------------------------------------------------
(cl-defmethod ofm-fix-overlap ((item ofm-item) item2 start)
  (when (or (string= (slot-value item 'type) "group")
            (string= (slot-value item 'type) "until"))
    (ofm-debug "check/fix overlap between %s and %s" (slot-value item 'id) (slot-value item2 'id))
    (let* ((M 20)
           (x11 (- (ofm-x item) M))
           (x12 (+ x11 (slot-value item 'w) M))
           (x21 (- (ofm-x item2) M))
           (x22 (+ x21 (slot-value item2 'w) M))
           (y11 (- (ofm-y item) M))
           (y12 (+ y11 (slot-value item 'h) M))
           (y21 (- (ofm-y item2) M))
           (y22 (+ y21 (slot-value item2 'h) M))
           (Overlap (and
                     (not (or (> x11 x22) (< x12 x21)))
                     (not (or (> y21 y12) (< y22 y11))))
                    )
           (dir (slot-value (ofm-item-from-id (nth 0 (slot-value item 'items))) 'dir))
           )
      (when Overlap
        (ofm-debug  "%s overlaps %s" (slot-value item 'id) (slot-value item2 'id))
        (let ((Delta (cond ((string= "LR" dir) (ofm-coords :x (- x22 x11) :y 0))
                           ((string= "RL" dir) (ofm-coords :x (- x21 x12) :y 0))
                           ((string= "TB" dir) (ofm-coords :x 0 :y (- y22 y11)))
                           ((string= "BT" dir) (ofm-coords :x 0 :y (- y12 y21)))
                           )
                     )
              )
          (when Delta (ofm-group-translate ofm-flowChart Delta start))
          )
        )
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : looking for the last real item in this group
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 23-17-2023 14:17:06
;;|--------------------------------------------------------------
(cl-defmethod ofm-last ((group ofm-group) )
  (ofm-last (ofm-item-from-id (car (last (slot-value group 'items)))))
  )
;;|--------------------------------------------------------------
;;|Description : looking for the first real item in this group
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 23-17-2023 14:17:06
;;|--------------------------------------------------------------
(cl-defmethod ofm-first ((group ofm-group) )
  (ofm-first (ofm-item-from-id (nth 0 (slot-value group 'items))))
  )
