;;|--------------------------------------------------------------
;;|Description : a rectangle class
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 10-08-2023 17:08:45
;;|--------------------------------------------------------------
(defclass ofm-pageNumber (ofm-item)
  ()
  "a rectangle class"
  )
(cl-defmethod initialize-instance :after ((item ofm-pageNumber) &rest args)
  (ofm-debug  "pagenumber constructor args=%s" args)
  (set-slot-value item 'type "pageNumber")
  (set-slot-value item 'w 20)
  (set-slot-value item 'h 20)
  (ofm-create-anchors item)
  )
;;|--------------------------------------------------------------
;;|Description : info
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 10-17-2023 17:17:08
;;|--------------------------------------------------------------
(cl-defmethod ofm-info ((item ofm-pageNumber) &optional text force)
  (let ((slots (list 'id
                     'type
                     'in
                     'out
                     'anchors
                     'w
                     'h
                     'text
                     'from
                     'dir
                     'bg
                     'align
                     'fromP
                     )
               )
        )
    (cl-call-next-method item text slots force)
    )
  )
;;|--------------------------------------------------------------
;;|Description : Draw the rectangle
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 11-39-2023 09:39:02
;;|--------------------------------------------------------------
(cl-defmethod ofm-draw-item ((item ofm-pageNumber) canvas)
  (ofm-info item "Draw pageNumber : ")
  (let* ((g (svg-node canvas 'g))
         (x (ofm-x item 'center))
         (y (ofm-y item 'center))
         (n (slot-value item 'text))
         )
    (ofm-svg-pageNumber canvas n 0 x y)
    (ofm-draw-connection item canvas)
    )
  ;; DEBUG
  (ofm-drawInOut item canvas)
  ;; END DEBUG
  )
