;;|--------------------------------------------------------------
;;|Description : a latex box
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 18-22-2023 10:22:45
;;|--------------------------------------------------------------
(push '("latex" . ("DIR" "FROM" "FROM_PORT")) ofm-item-inputs)
(defclass ofm-latex (ofm-item)
  ((svg-data :initarg :svg-data :initform nil)
   (dx :initarg :dx :initform 0)
   (dy :initarg :dy :initform 0)
   )
  "a latex box"
  )
(cl-defmethod initialize-instance :after ((item ofm-latex) &rest args)
  (ofm-debug  "latex constructor args=%s" args)
  (set-slot-value item 'type "latex")
  (ofm-latex-compile item)
  (ofm-create-anchors item)
  )
;;|--------------------------------------------------------------
;;|Description : info
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 10-17-2023 17:17:08
;;|--------------------------------------------------------------
(cl-defmethod ofm-info ((item ofm-latex) &optional text force)
  (let ((slots (list 'id
                     'type
                     'in
                     'out
                     'anchors
                     'w
                     'h
                     'text
                     'from
                     'dir
                     'bg
                     'align
                     )
               )
        )
    (cl-call-next-method item text slots force)
    )
  )
;;|--------------------------------------------------------------
;;|Description : create a binary from the latex
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 18-53-2023 10:53:29
;;|--------------------------------------------------------------
(cl-defmethod ofm-latex-compile ((item ofm-latex))
  ;; We need to adapt the id tags in the svg file because they need to be unique on a page
  ;; We also need to remove the newlines from the svg file
  (let* ((root (format "/tmp/block_%s" (slot-value item 'id)))
         (texFile (format "%s.tex" root))
         (dviFile (format "%s.dvi" root))
         (svgFile (format "%s.svg" root))
         (latexCmd (format "latex -output-directory=/tmp %s;dvisvgm -v0 -n1 -o%s %s"
                           texFile svgFile dviFile))
         (id (slot-value item 'id))
         (fixCmd (format "sed -z -i -E -e \"s/id='([^']*)'/id='\\1_%s'/g\" -e \"s/\\n//g\" %s" id svgFile))
         )
    (shell-command (format "rm -f %s.*" root))
    (with-temp-file texFile
      (insert "\\documentclass[crop=true,border=1pt,varwidth]{standalone}\n")
      (insert "\\batchmode\n")
      (insert "\\usepackage[utf8]{inputenc}\n")
      (insert "\\usepackage{amsmath}\n")
      (insert "\\usepackage{paralist}\n")
      (insert "\\usepackage[svgnames]{xcolor}\n")
      (insert "\\makeatletter\n")
      (insert "\\newcommand{\\myCol}[2]{\\color{#1} #2 \\color{Black}}\n")
      (insert "\\newcommand{\\Green}[1]{\\color{DarkGreen} #1 \\color{Black}}\n")
      (insert "\\newcommand{\\Red}[1]{\\color{Red} #1 \\color{Black}}\n")
      (insert "\\newcommand{\\Blue}[1]{\\color{Blue} #1 \\color{Black}}\n")
      (insert "\\newcommand{\\Orange}[1]{\\color{Orange} #1 \\color{Black}}\n")
      (insert "\\newcommand{\\White}[1]{\\color{White} #1 \\color{Black}}\n")
      (insert "\\makeatother\n")
      (insert "\\begin{document}\n")
      (cl-loop for l in (slot-value item 'lines) do
               (insert l)
               )
      (insert "\\end{document}\n")
      )
    (shell-command latexCmd)
    (shell-command fixCmd)
    (let* ((xml (car (xml-parse-file svgFile)))
           (BB (mapcar 'string-to-number (split-string (cdr (assq 'viewBox (xml-node-attributes xml))))))
           (dx (nth 0 BB))
           (dy (nth 1 BB))
           (width (+ ofm-margin (nth 2 BB)))
           (height (+ ofm-margin (nth 3 BB)))
           (svg-nodes (xml-node-children xml))
           )
      (set-slot-value item 'w width)
      (set-slot-value item 'h height)
      (set-slot-value item 'dx dx)
      (set-slot-value item 'dy dy)
      (set-slot-value item 'svg-data svg-nodes)
      )
    )
  )

;;|--------------------------------------------------------------
;;|Description : Draw the latex box
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 18-31-2023 10:31:57
;;|--------------------------------------------------------------
(cl-defmethod ofm-draw-item ((item ofm-latex) canvas)
  (ofm-info item "Draw latex : ")
  (let* ((x (ofm-x item))
         (y (ofm-y item))
         (w (slot-value item 'w))
         (h (slot-value item 'h))
         (x2 (+ (* 0.5 ofm-margin) (- x (slot-value item 'dx))))
         (y2 (+ (* 0.5 ofm-margin) (- y (slot-value item 'dy))))
         (g (svg-node canvas 'g))
         (grect (svg-node g 'g))
         (gsvg  (svg-node g 'g :transform (format "translate(%s %s)" x2 y2) :stroke "none"))
         )
    (svg-rectangle grect x y w h
                   :fill-opacity 0.5
                   :fill (slot-value item 'bg)
                   )
    (ofm-item-draw-shade item grect)
    (cl-loop for n in (slot-value item 'svg-data) do
             (svg--append gsvg n)
             )
    (ofm-draw-connection item canvas)
    )
  (ofm-drawInOut item canvas)
  )
