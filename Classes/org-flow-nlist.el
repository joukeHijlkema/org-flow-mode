;;|--------------------------------------------------------------
;;|Description : A list class
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 18-12-2023 11:12:33
;;|--------------------------------------------------------------
(push '("nlist" . ("DIR" "FROM" "FROM_PORT")) ofm-item-inputs)
(defclass ofm-nlist (ofm-item)
  ()
  "A list class"
  )
;;|--------------------------------------------------------------
;;|Description : constructor
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 18-12-2023 11:12:59
;;|--------------------------------------------------------------
(cl-defmethod initialize-instance :after ((item ofm-nlist) &rest args)
  (ofm-debug "nlist constructor args=%s" args)
  (set-slot-value item 'type "nlist")
  (set-slot-value item 'w (+ (slot-value item 'w) (nth 0 (ofm-svg-textBB "1. "))))
  (ofm-create-anchors item)
  )
;;|--------------------------------------------------------------
;;|Description : info
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 10-17-2023 17:17:08
;;|--------------------------------------------------------------
(cl-defmethod ofm-info ((item ofm-nlist) &optional text force)
  (let ((slots (list 'id
                     'type
                     'in
                     'out
                     'anchors
                     'w
                     'h
                     'text
                     'from
                     'dir
                     'bg
                     'align
                     )
               )
        )
    (cl-call-next-method item text slots force)
    )
  )
;;|--------------------------------------------------------------
;;|Description : Draw the rectangle
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 11-39-2023 09:39:02
;;|--------------------------------------------------------------
(cl-defmethod ofm-draw-item ((item ofm-nlist) canvas)
  (ofm-info item "Draw list : ")
  (let* ((g (svg-node canvas 'g))
         (x (ofm-x item))
         (y (ofm-y item))
         (w (slot-value item 'w))
         (h (slot-value item 'h))
         (xc (+ x 2))
         (nl (length (slot-value item 'lines)))
         (D  (/ h nl))
         (yc (+ y (* 0.7 D)))
         )
    (ofm-item-draw-shade item g)
    (svg-rectangle g x y w h
                   :fill-opacity 0.5
                   :fill (slot-value item 'bg)
                   )
    (ofm-svg-nlist g (slot-value item 'lines)
                  :x xc
                  :y yc
                  :text-anchor "start"
                  :text-align (slot-value item 'align)
                  :alignment-baseline "middle"
                  :fill "black"
                  :stroke "none"
                  )
    (ofm-draw-connection item canvas)
    )
  ;; DEBUG
  (ofm-drawInOut item canvas)
  ;; END DEBUG
  )
