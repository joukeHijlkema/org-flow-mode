;;|--------------------------------------------------------------
;;|Description : a rectangle class
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 10-08-2023 17:08:45
;;|--------------------------------------------------------------
(push '("rect" . ("DIR" "FROM" "FROM_PORT" "BACKGROUND")) ofm-item-inputs)
(defclass ofm-rect (ofm-item)
  ()
  "a rectangle class"
  )
(cl-defmethod initialize-instance :after ((item ofm-rect) &rest args)
  (ofm-debug  "rect constructor args=%s" args)
  (set-slot-value item 'type "rect")
  (ofm-create-anchors item)
  )
;;|--------------------------------------------------------------
;;|Description : info
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 10-17-2023 17:17:08
;;|--------------------------------------------------------------
(cl-defmethod ofm-info ((item ofm-rect) &optional text force)
  (let ((slots (list 'id
                     'type
                     'w
                     'h
                     'anchors
                     'text
                     'from
                     'fromP
                     'dir
                     'bg
                     'align
                     )
               )
        )
    (cl-call-next-method item text slots force)
    )
  )
;;|--------------------------------------------------------------
;;|Description : Draw the rectangle
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 11-39-2023 09:39:02
;;|--------------------------------------------------------------
(cl-defmethod ofm-draw-item ((item ofm-rect) canvas)
  (ofm-info item "Draw rect : ")
  (let* ((g (svg-node canvas 'g))
         (w (slot-value item 'w))
         (h (slot-value item 'h))
         (x (ofm-x item))
         (y (ofm-y item))
         (xc (+ x (* 0.5 w)))
         (nl (length (slot-value item 'lines)))
         (D  (/ h nl))
         (yc (+ y (* 0.5 D)))
         )
    (ofm-item-draw-shade item g)
    (svg-rectangle g x y w h
                   :fill-opacity 0.5
                   :fill (slot-value item 'bg)
                   )
    (ofm-svg-text g (slot-value item 'lines)
                  :x xc
                  :y yc
                  :text-anchor "middle"
                  :text-align (slot-value item 'align)
                  :alignment-baseline "middle"
                  :fill "black"
                  :stroke "none"
                  )
    (ofm-draw-connection item canvas)
    )
  ;; DEBUG
  (ofm-drawInOut item canvas)
  ;; END DEBUG
  )
