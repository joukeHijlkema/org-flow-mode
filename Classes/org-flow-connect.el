;;|--------------------------------------------------------------
;;|Description : an arrow connecting 2 items
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 16-23-2024 19:23:13
;;|--------------------------------------------------------------

(push '("connect" . ("FROM" "TO" "FROM_PORT" "TO_PORT" "D")) ofm-item-inputs)

(defclass ofm-connect (ofm-item)
  ((to     :initarg :to     :initform nil)
   (tPort  :initarg :tPort  :initform (ofm-coords :x 0 :y 0) :type ofm-coords) 
   (fPort  :initarg :fPort  :initform (ofm-coords :x 0 :y 0) :type ofm-coords)
   (d      :initarg :d      :initform nil)
   (layout :initarg :layout :initform nil)
   (D1     :initarg :D1 :   :initform (ofm-coords :x 0 :y 0) :type ofm-coords)
   (D2     :initarg :D2 :   :initform (ofm-coords :x 0 :y 0) :type ofm-coords)
   )
  "an arrow connecting 2 items"
  )
(cl-defmethod initialize-instance :after ((item ofm-connect) &rest args)
  (ofm-debug "connect constructor args=%s" args)
  (set-slot-value item 'type "arrow")
  (set-slot-value item 'baseType "arrow")
  (let* ((to   (ofm-item-from-id (or (plist-get args :to)   (org-entry-get (point) "TO"  ))))
         (from (ofm-item-from-id (or (plist-get args :from) (org-entry-get (point) "FROM"))))
         (tPort (or (plist-get args :tPort) (org-entry-get (point) "TO_PORT"))) 
         (fPort (or (plist-get args :fPort) (org-entry-get (point) "FROM_PORT")))
         (d     (or (plist-get args :d)     (org-entry-get (point) "D")))
         (layout "")
         )
    (set-slot-value item 'to to)
    (set-slot-value item 'from from)
    (when d (set-slot-value item 'd (string-to-number d)))
    (when fPort
      (set-slot-value item 'fPort (ofm-a from (intern fPort)))
      (set-slot-value item 'layout (concat (slot-value item 'layout) fPort))
      )
    (when tPort
      (set-slot-value item 'tPort (ofm-a to   (intern tPort)))
      (set-slot-value item 'layout (concat (slot-value item 'layout) tPort))
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : calculate the BB
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 04-10-2024 18:10:10
;;|--------------------------------------------------------------
(cl-defmethod ofm-connect-update ((item ofm-connect))
  (let* ((ps (slot-value item 'fPort))
         (pe (slot-value item 'tPort))
         (d (or (slot-value item 'd) (* 0.5 ofm-arrow-length)))
         (D1 (cond
              ((string= (slot-value item 'layout) "northnorth")
               (ofm-coords :x 0 :y (- (ofm-Y pe) (ofm-Y ps) d)))
              ((string= (slot-value item 'layout) "southsouth")
               (ofm-coords :x 0 :y (+ d (max 0 (- (ofm-Y pe) (ofm-Y ps))))))
              ((string= (slot-value item 'layout) "southeast")
               (ofm-coords :x 0 :y (+ d (max 0 (- (ofm-Y pe) (ofm-Y ps))))))
              ((string= (slot-value item 'layout) "northeast")
               (ofm-coords :x 0 :y (- (ofm-Y pe) (ofm-Y ps))))
              ((string= (slot-value item 'layout) "easteast")
               (ofm-coords :x (+ d (max 0 (- (ofm-X pe) (ofm-X ps)))) :y 0))
              ((string= (slot-value item 'layout) "eastsouth")
               (ofm-coords :x (- (ofm-X pe) (ofm-X ps)) :y 0))
              (t (ofm-coords :x 0 :y 0))
              )
             )
         (D2 (cond
              ((string= (slot-value item 'layout) "northnorth")
               (ofm-coords :x (- (ofm-X pe) (ofm-X ps)) :y 0))
              ((string= (slot-value item 'layout) "southsouth")
               (ofm-coords :x (- (ofm-X pe) (ofm-X ps)) :y 0))
              ((string= (slot-value item 'layout) "easteast")
               (ofm-coords :x 0 :y (- (ofm-Y pe) (ofm-Y ps))))
              ((string= (slot-value item 'layout) "eastsouth")
               (ofm-coords :x 0 :y 0))
              ((string= (slot-value item 'layout) "northeast")
               (ofm-coords :x 0 :y 0))
              (t (ofm-coords :x 0 :y 0))
              )
             )
         (p2 (ofm-coord-plus ps D1 1 t))
         (p3 (ofm-coord-plus p2 D2 1 t))
         (BB (ofm-coords-BB (list ps p2 p3 pe)))
         (C  (ofm-coords :x (+ (nth 0 BB) (* 0.5 (nth 2 BB)))
                         :y (+ (nth 1 BB) (* 0.5 (nth 3 BB)))))
         )
    (set-slot-value item 'D1 D1)
    (set-slot-value item 'D2 D2)
    (set-slot-value item 'w (nth 2 BB))
    (set-slot-value item 'h (nth 3 BB))
    (ofm-calc-anchors (slot-value item 'anchors) item)
    (ofm-move-anchors (slot-value item 'anchors) C)
    )
  )
;;|--------------------------------------------------------------
;;|Description : arrow info
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 16-34-2024 19:34:57
;;|--------------------------------------------------------------
(cl-defmethod ofm-info ((item ofm-connect) &optional text force)
  (let ((slots (list 'id
                    'type
                    'w
                    'h
                    'anchors
                    'from
                    'to
                    )
               )
        )
    (cl-call-next-method item text slots force)
    )
  )
;;|--------------------------------------------------------------
;;|Description : draw the connector
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 29-21-2024 16:21:33
;;|--------------------------------------------------------------
(cl-defmethod ofm-draw-item ((item ofm-connect) canvas)
  (ofm-info item "Draw connect : ")
  (let* ((g (svg-node canvas 'g))
         (ps (slot-value item 'fPort))
         (pe (slot-value item 'tPort))
         (d (or (slot-value item 'd) (* 0.5 ofm-arrow-length)))
         (D1 (slot-value item 'D1))
         (D2 (slot-value item 'D2))
         (p2 (ofm-coord-plus ps D1 1 t))
         (p3 (ofm-coord-plus p2 D2 1 t))
         )
    (ofm-arrow ps pe g `(,p2 ,p3))
    )
  )
(provide 'org-flow-connect)
