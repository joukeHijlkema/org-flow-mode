;;|--------------------------------------------------------------
;;|Description : A while loop
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-28-2023 16:28:06
;;|--------------------------------------------------------------
(push '("while" . ("DIR" "FROM" "FROM_PORT" "SEUL" "INVERSE")) ofm-item-inputs)
(defclass ofm-while (ofm-group)
  ((dec :initarg :dec)
   (end :initarg :end)
   )
  "A while loop"
  )
(cl-defmethod initialize-instance :after ((item ofm-while) &rest args)
  (ofm-debug  "while constructor")
  (set-slot-value item 'type "while")
  (let* ((id (org-id-new))
         (First (ofm-item-from-id (nth 0 (slot-value item 'items))))
         (Last (ofm-item-from-id (car (last (slot-value item 'items)))))
         (dec (ofm-decision :text (slot-value item 'text)
                            :id id
                            :bg (or (org-entry-get (point) "BACKGROUND") "green")
                            :dir (if Last (slot-value Last 'dir) nil)
                            :seul nil
                            :from (slot-value First 'from)
                            )
              )
         )
    (set-slot-value item 'end Last)
    (ofm-addItem item dec nil)
    (set-slot-value item 'dec dec)
    (set-slot-value First 'from id)
    (ofm-create-anchors item)
    )
  )
;;|--------------------------------------------------------------
;;|Description : define in and output(s)
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 15-10-2024 14:10:38
;;|--------------------------------------------------------------
(cl-defmethod ofm-make-io ((item ofm-while) )
  (let* ((dir (slot-value item 'dir))
         (in  (cond ((string-match-p "TB" dir) (ofm-a (slot-value item 'dec) 'north))
                    ((string-match-p "BT" dir) (ofm-a (slot-value item 'dec) 'south))
                    ((string-match-p "LR" dir) (ofm-a (slot-value item 'dec) 'west))
                    ((string-match-p "RL" dir) (ofm-a (slot-value item 'dec) 'east))
                    )
              )
         (out (cond ((string-match-p "TB" dir) (ofm-coord-plus
                                                (ofm-a (slot-value item 'end) 'south)
                                                (ofm-coords :x 0 :y 10)
                                                1.0 t
                                                ))
                    ((string-match-p "BT" dir) (ofm-coord-plus
                                                (ofm-a (slot-value item 'end) 'north)
                                                (ofm-coords :x 0 :y 10)
                                                1.0 t
                                                )
                     )
                    )
              )
         )
    (set-slot-value (slot-value item 'anchors) 'in in)
    (set-slot-value (slot-value item 'anchors) 'out out)
    )
  )
;;|--------------------------------------------------------------
;;|Description : Draw the while block
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-07-2023 17:07:56
;;|--------------------------------------------------------------
(cl-defmethod ofm-draw-item ((item ofm-while) canvas)
  (ofm-info item "draw until")
  (ofm-make-io item)
  (let* ((g (svg-node canvas 'g))
         (dec (slot-value item 'dec))
         (from (ofm-from dec))
         (dir (slot-value from 'dir))
         (end (slot-value item 'end))
         (s (cond ((string-match-p "T" dir) (ofm-a end 'west))
                  ((string-match-p "L" dir) (ofm-a end 'south))
                  )
            )
         (e (cond ((string-match-p "T" dir) (ofm-a dec 'west))
                  ((string-match-p "L" dir) (ofm-a dec 'south))
                  )
            )
         (dx (+ 10 (* 0.5 (- (max (slot-value dec 'w) (slot-value end 'w)) (slot-value dec 'w)))))
         (dy (+ 10 (* 0.5 (- (max (slot-value dec 'h) (slot-value end 'h)) (slot-value dec 'h)))))
         (d1 (cond ((string-match-p "T" dir) (ofm-coords :x (- dx) :y 0))
                   ((string-match-p "L" dir) (ofm-coords :x 0 :y (- dy)))
                   )
             )
         (d2 (cond ((string-match-p "T" dir) (ofm-coords :x 0 :y (ofm-coord-dy s e)))
                   ((string-match-p "L" dir) (ofm-coords :x (ofm-coord-dx s e) :y 0))
                   )
             )
         (s2 (cond ((string= "TB" dir) (ofm-coord-plus s d1 1 t))
                   ((string= "BT" dir) (ofm-coord-plus s d1 1 t))
                   ((string= "LR" dir) (ofm-coord-min s d1 1 t))
                   ((string= "RL" dir) (ofm-coord-min s d1 1 t))
                   )
             )
         (s3 (ofm-coord-plus s2 d2 1 t))
         (s4 (cond ((string-match-p "T" dir) (ofm-a dec 'east))
                   ((string-match-p "L" dir) (ofm-a dec 'south))
                   )
             )
         (s5 (cond ((string-match-p "T" dir) (ofm-coord-plus s4
                                                             (ofm-coord-min-x (ofm-a item 'east) s4)
                                                             1.0 t)
                    )
                   )
             )
         
         (s6 (cond ((string-match-p "T" dir) (ofm-coord-plus s5
                                                             (ofm-coord-min-y (ofm-a item 'out) s5)
                                                             1.0 t)
                    )
                   )
             )
         )
    (cl-loop for i in (slot-value item 'items) do (ofm-draw-item (ofm-item-from-id i) g))
    (ofm-arrow s e g `(,s2 ,s3))
    (ofm-line s4 (ofm-a item 'out) g `(,s5 ,s6))
    )
  )
