;;|--------------------------------------------------------------
;;|Description : basic item class
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 25-31-2023 16:31:56
;;|--------------------------------------------------------------
(defclass ofm-item ()
  ((text   :initarg :text
           :initform "")
   (id     :initarg :id
           :initform "id")
   (type   :initarg :type
           :initform nil)
   (baseType :initarg baseType
             :initform "item"
             :type string)
   (lines  :initarg :lines
           :initform '())
   (from   :initarg :from
           :initform nil)
   (dir    :initarg :dir
           :initform nil)
   (w      :initarg :w
           :initform 0)
   (h      :initarg :h
           :initform 0)
   (dw     :initarg dw
           :initform 0)
   (dh     :initarg dh
           :initform 0)
   (in     :initarg in
           :initform (ofm-coords :x 0 :y 0)
           :type ofm-coords)
   (out    :initarg out
           :initform (ofm-coords :x 0 :y 0)
           :type ofm-coords)
   (fromP  :initarg :fromP
           :initform "out")
   (bg     :initarg :bg
           :initform "white")
   (lar    :initarg :lar
           :initform nil)
   (level  :initarg :level
           :initform 0)
   (align  :initarg :align
           :initform "center")
   (anchors :initarg anchors
            :initform (ofm-anchors)
            :type ofm-anchors)
   )
  "basic item class"
  )
(cl-defmethod initialize-instance ((item ofm-item) &rest args)
 (ofm-debug "item constructor args=%s" args)
 (let* ((args (nth 0 args))
        (txt  (or (plist-get args :text) (ofm-get-subtree-contents)))
        (id   (or (plist-get args :id) (org-entry-get (point) "ID") (org-id-new)))
        (from (or (plist-get args :from) (org-entry-get (point) "FROM")))
        (dir  (or (plist-get args :dir) (org-entry-get (point) "DIR")))
        (bg   (or (plist-get args :bg) (org-entry-get (point) "BACKGROUND")))
        (lar  (or (plist-get args :lar)
                  (string-to-number 
                   (format "%s"
                           (or (org-entry-get (point) "ARROW_LENGTH") ofm-arrow-length)
                           )
                   )
                  ) ;; it is either a string or a number ...
              )
        (align (or (plist-get args :align) (org-entry-get (point) "ALIGN")))
        (parts (cl-loop for p in (split-string txt "\n") collect p))
        (BB (ofm-svg-textBB txt))
        (w (+ ofm-margin (nth 0 BB)))
        (h (+ ofm-margin (nth 1 BB)))
        (C (ofm-coords :x (* 0.5 w) :y (* 0.5 h)))
        )
   (set-slot-value item 'id id)
   (set-slot-value item 'text txt)
   (set-slot-value item 'w w)
   (set-slot-value item 'h h)
   (set-slot-value item 'anchors (ofm-anchors))
   (set-slot-value item 'lines parts)
   (set-slot-value item 'level (org-current-level))
   (when (org-entry-get (point) "FROM_PORT") (set-slot-value item 'fromP (org-entry-get (point) "FROM_PORT")))
   (when from (set-slot-value item 'from from))
   (when dir (set-slot-value item 'dir dir))
   (when bg (set-slot-value item 'bg bg))
   (when lar (set-slot-value item 'lar lar))
   (when align (set-slot-value item 'align align))
   )
 )
;;|--------------------------------------------------------------
;;|Description : parse the item
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 06-24-2023 15:24:07
;;|--------------------------------------------------------------
(cl-defmethod  ofm-item-parse (target)
  "parse the item"
  (warning "This should be done in the derived classes !!")
  )
;;|--------------------------------------------------------------
;;|Description : item info
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 13-49-2023 10:49:16
;;|--------------------------------------------------------------
(cl-defmethod ofm-info ((item ofm-item) &optional text list force)
  (when (or force (> ofm-verbose 1))
    (if text (message "====== %s =======" text))
    (if ofm-verbose
        (cl-loop for s in list
                 do (message "= slot %s %s"
                             s
                             (if (and (slot-exists-p item s) (slot-boundp item s))
                                 (if (string= "anchors" s)
                                     (ofm-anchors-info (slot-value item 'anchors))
                                   (slot-value item s)
                                   )
                               "none")
                             )
                 )
      )
    (if text (message "====== END %s =======" text))
    )
  )
;;|--------------------------------------------------------------
;;|Description : position the item relative from FROM
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 29-56-2023 14:56:48
;;|--------------------------------------------------------------
(cl-defmethod ofm-position ((item ofm-item) &optional offset)
  (when (and (string= "item" (slot-value item 'baseType)) (slot-value item 'from))
    (let* ((from (ofm-from item))
           (dir (slot-value item 'dir))
           (lar (slot-value item 'lar))
           (D0 (or offset (ofm-coords :x 0 :y 0)))
           (D1 (ofm-coord-plus D0 (ofm-coord-min
                                   (ofm-a from (intern (slot-value item 'fromP)))
                                   (ofm-a item 'in) 1.0 t)
                               1 t))
           (D2 (cond ((string= dir "LR") (ofm-coord-plus D1 (ofm-coords :x lar     :y 0      ) 1 t))
                     ((string= dir "RL") (ofm-coord-plus D1 (ofm-coords :x (- lar) :y 0      ) 1 t))
                     ((string= dir "TB") (ofm-coord-plus D1 (ofm-coords :x 0       :y lar    ) 1 t))
                     ((string= dir "BT") (ofm-coord-plus D1 (ofm-coords :x 0       :y (- lar)) 1 t))
                     )
               )
           )
      (ofm-translate-item item D2)
      )
    )
  (ofm-debug "DONE")
  )
;;|--------------------------------------------------------------
;;|Description : Create the anchors
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 11-04-2023 11:04:17
;;|--------------------------------------------------------------
2(cl-defmethod ofm-create-anchors ((item ofm-item))
  (ofm-debug "Anchors item: ")
  (ofm-info item "Before Anchor: ")
  (ofm-calc-anchors (slot-value item 'anchors) item)
  (ofm-info item "After Anchor: ")
  )
;;|--------------------------------------------------------------
;;|Description : Translate an item
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 11-04-2023 11:04:29
;;|--------------------------------------------------------------
(cl-defmethod ofm-translate-item ((item ofm-item) C &optional noKids)
  (ofm-debug "== translate %s with %s" (slot-value item 'id) C)
  (ofm-translate-anchors (slot-value item 'anchors) C)
  (if (and (not noKids) (slot-exists-p item 'items))
      (cl-loop for k in (slot-value item 'items) do (ofm-translate-item (ofm-item-from-id k) C))
    )
  )
;;|--------------------------------------------------------------
;;|Description : move item ancher to target
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 29-35-2024 12:35:15
;;|--------------------------------------------------------------
(cl-defmethod ofm-move-item ((item ofm-item) ancher target &optional Delta)
  "move item ancher to target"
  (let* ((C1 (ofm-a item ancher))
         (D1 (or Delta (ofm-coords :x 0 :y 0))) 
         (C2 (ofm-coord-plus target D1 1 t))
         (D  (ofm-coord-min C2 C1 1 t)))
    (ofm-translate-item item D)
    )
  )
;; === Shortcuts ===
(cl-defmethod ofm-a ((item ofm-item) anchor)
  (slot-value (slot-value item 'anchors) anchor)
  )
(cl-defmethod ofm-x ((item ofm-item) &optional anchor)
  (slot-value (ofm-a item (or anchor 'nw)) 'x)
  )
(cl-defmethod ofm-y ((item ofm-item) &optional anchor)
  (slot-value (ofm-a item (or anchor 'nw)) 'y)
  )
;;|--------------------------------------------------------------
;;|Description : return the item in the 'from slot
;;|NOTE : this is needed to alow from alternative froms
;;|-
;;|Author : jouke hylkema
;;|date   : 01-58-2023 16:58:20
;;|--------------------------------------------------------------
(cl-defmethod ofm-from ((item ofm-item))
  "return the item in the 'from slot"
  (cond ((slot-value item 'from)
         (let ((from (if (stringp (slot-value item 'from))
                         (ofm-item-from-id (slot-value item 'from))
                       (slot-value item 'from)
                       )
                     )
               )
           (cond ((not from)
                  (ofm-debug "item %s (%s) has no 'from slot" (slot-value item 'id) (slot-value item 'type))
                  nil)
                 ((string= "until" (slot-value from 'type)) (slot-value from 'dec))
                 (t from)
                 )
           )
         )
        (t 
         (ofm-debug "item %s has no 'from slot" (slot-value item 'id))
         nil
         )
        )
  )
;;|--------------------------------------------------------------
;;|Description : return the output as a function of dir
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 10-27-2023 13:27:53
;;|--------------------------------------------------------------
(cl-defmethod ofm-item-out ((item ofm-item) dir)
  (cond ((string= dir "LR") (ofm-a item 'east))
        ((string= dir "RL") (ofm-a item 'west))
        ((string= dir "TB") (ofm-a item 'south))
        ((string= dir "BT") (ofm-a item 'north)))
  )
;;|--------------------------------------------------------------
;;|Description : return the input as a function of dir
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 10-27-2023 13:27:53
;;|--------------------------------------------------------------
(cl-defmethod ofm-item-in ((item ofm-item) dir)
  (cond ((string= dir "LR") (ofm-a item 'west))
        ((string= dir "RL") (ofm-a item 'east))
        ((string= dir "TB") (ofm-a item 'north))
        ((string= dir "BT") (ofm-a item 'south)))
  )
(cl-defmethod ofm-draw-connection ((item ofm-item) canvas &optional from)
  (ofm-info item "draw-connection")
  (if (or from (ofm-from item))
      (let* ((from (or from (ofm-from item)))
             (c1 (ofm-a from (intern (slot-value item 'fromP))))
             (c2 (ofm-a item 'in))
             )
        (ofm-arrow c1 c2 g)
        )
    )
  )
;;|--------------------------------------------------------------
;;|Description : draw in and out anchors
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 11-04-2023 10:04:52
;;|--------------------------------------------------------------
(cl-defmethod ofm-drawInOut ((item ofm-item) canvas)
  (ofm-info item "connectors")
  (if (> ofm-verbose 0)
      (let* ((g (svg-node canvas 'g))
             (xi (ofm-x item 'in))
             (yi (ofm-y item 'in))
             (xo (ofm-x item 'out))
             (yo (ofm-y item 'out))
             )
        (svg-circle g xi yi 4 :fill "red")
        (svg-circle g xo yo 4 :fill "blue")
        )
    )
  )
;;|--------------------------------------------------------------
;;|Description : this returns the item when it is the last in a group
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 23-17-2023 14:17:06
;;|--------------------------------------------------------------
(cl-defmethod ofm-last ((item ofm-item) )
  item
  )
;;|--------------------------------------------------------------
;;|Description : this returns the item when it is the first in a group
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 23-17-2023 14:17:06
;;|--------------------------------------------------------------
(cl-defmethod ofm-first ((item ofm-item) )
  item
  )
;;|--------------------------------------------------------------
;;|Description : draw a shaed for this itme
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 27-05-2023 18:05:44
;;|--------------------------------------------------------------
(cl-defmethod ofm-item-draw-shade ((item ofm-item) canvas)
  (if ofm-shadows
      (let* ((w (slot-value item 'w))
             (h (slot-value item 'h))
             (x (ofm-x item))
             (y (ofm-y item))
             (sw 2)
             (sd 5)
             (xs1 (+ x sd))
             (xs2 (+ x w sw))
             (ys1 (+ y h sw))
             (ys2 (+ y sd))
             )
        (svg-polyline canvas `((,xs1 . ,ys1)
                               (,xs2 . ,ys1)
                               (,xs2 . ,ys2))
                      :stroke-width (* 2 sw)
                      :stroke "#4e4e4e"
                      :fill "none")
        )
    )
  )

(provide 'org-flow-items)
