;;|--------------------------------------------------------------
;;|Description : a decision class
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 30-24-2023 18:24:35
;;|--------------------------------------------------------------
(push '("decision" . ("DIR" "FROM" "SEUL" "INVERSE")) ofm-item-inputs)

(defclass ofm-decision (ofm-item)
  ((seul :initarg :seul :initform t)
   (inverse :initarg :inverse :initform nil)
   )
  "a decision class"
  )
(cl-defmethod initialize-instance :after ((item ofm-decision) &rest args)
  (ofm-debug "decision constructor args=%s" (car args))
  (set-slot-value item 'type "decision")
  (set-slot-value item 'bg (or (plist-get (car args) :bg) "green"))
  (if (string-match-p "T" (slot-value item 'dir)) (set-slot-value item 'dw 20))
  (if (string-match-p "R" (slot-value item 'dir)) (set-slot-value item 'dh 20))
  (set-slot-value item 'seul (org-entry-get (point) "SEUL"))
  (set-slot-value item 'inverse (org-entry-get (point) "INVERSE"))

  ;; we have to adapt the anchors because of the shape
  (set-slot-value item 'w (+ (slot-value item 'w) (slot-value item 'h)))
  (ofm-create-anchors item)
  )
;;|--------------------------------------------------------------
;;|Description : info
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 10-17-2023 17:17:08
;;|--------------------------------------------------------------
(cl-defmethod ofm-info ((item ofm-decision) &optional text force)
  (let ((slots (list 'id
                     'type
                     'in
                     'out
                     'anchors
                     'w
                     'h
                     'dw
                     'dh
                     'text
                     'from
                     `dir
                     'bg
                     'seul
                     )
               )
        )
    (cl-call-next-method item text slots)
    )
  )
;;|--------------------------------------------------------------
;;|Description : return the output as a function of dir
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 10-27-2023 13:27:53
;;|--------------------------------------------------------------
(cl-defmethod ofm-item-out ((item ofm-decision) dir)
  (if (slot-value item :seul)
      (cl-call-next-method)
    (cond ((string= (slot-value item 'dir) "TB") (ofm-a item 'south))
          ((string= (slot-value item 'dir) "BT") (ofm-a item 'south))
          ((string= (slot-value item 'dir) "LR") (ofm-a item 'east))
          ((string= (slot-value item 'dir) "RL") (ofm-a item 'east))
          )
    )
  )
;;|--------------------------------------------------------------
;;|Description : draw the decision item
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 30-25-2023 18:25:21
;;|--------------------------------------------------------------
(cl-defmethod ofm-draw-item ((item ofm-decision) canvas)
  (ofm-info item "Draw decision: ")
  (let* ((g (svg-node canvas 'g))
         (x (ofm-x item))
         (y (ofm-y item))
         (w (slot-value item 'w))
         (h (slot-value item 'h))
         (h2 (* 0.5 h))
         (w2 (- (* 0.5 w) h2))
         (xc (ofm-x item 'center))
         (yc (ofm-y item 'center))
         (P `((,(- xc w2)    . ,(+ yc h2))
              (,(+ xc w2)    . ,(+ yc h2))
              (,(+ xc w2 h2) . ,yc)
              (,(+ xc w2)    . ,(- yc h2))
              (,(- xc w2)    . ,(- yc h2))
              (,(- xc w2 h2) . ,yc)
              (,(- xc w2)    . ,(+ yc h2))
              )
            )
         (dir (slot-value item 'dir))
         (Y (cond ((string= "TB" dir) `(,(+ (ofm-x item 'south) 2) ,(+ (ofm-y item 'south) 5) "start" "hanging"))
                  ((string= "BT" dir) `(,(+ (ofm-x item 'north) 2) ,(- (ofm-y item 'north) 5) "start" "baseline"))
                  ((string= "LR" dir) `(,(+ (ofm-x item 'east ) 2) ,(- (ofm-y item 'east ) 4) "start" "auto"))
                  ((string= "RL" dir) `(,(- (ofm-x item 'west ) 2) ,(- (ofm-y item 'west ) 4) "end" "auto"))
                  )
            )
         (sideX (cond ((string-match-p "T" dir) (if (slot-value item 'inverse) (- (ofm-x item 'west)  12) (+ (ofm-x item 'east) 2)))
                      ((string-match-p "L" dir) (if (slot-value item 'inverse) (+ (ofm-x item 'north) 1) (+ (ofm-x item 'south) 2)))
                      )
               ) ;; Alow for placement of the no
         (sideY (cond ((string-match-p "T" dir) (if (slot-value item 'inverse) (- (ofm-y item 'west)  3) (- (ofm-y item 'east)  2)))
                      ((string-match-p "L" dir) (if (slot-value item 'inverse) (- (ofm-y item 'north) 10) (+ (ofm-y item 'south) 5)))
                      )
                ) ;; Alow for placement of the no
         (N (cond ((string= "TB" dir) `(,sideX ,sideY "start" "auto"))
                  ((string= "BT" dir) `(,sideX ,sideY "start" "auto"))
                  ((string= "LR" dir) `(,sideX ,sideY "start" "hanging"))
                  ((string= "RL" dir) `(,sideX ,sideY "start" "hanging"))
                  )
            )
         )
    (ofm-item-draw-shade item g)
    (svg-polyline g P
                  :fill-opacity 0.5
                  :fill (slot-value item 'bg)
                  :stroke "black"
                  :stroke-width 1
                  :stroke-linecap "round" 
                  )
    (ofm-svg-text g (slot-value item 'lines)
                  :x xc
                  :y yc
                  :text-anchor "middle"
                  :alignment-baseline "middle"
                  :dominant-baseline "middle"
                  :fill "black"
                  :stroke "none"
                  )
    (ofm-svg-text g '("yes")
                  :x (nth 0 Y)
                  :y (nth 1 Y)
                  :text-anchor (nth 2 Y)
                  :dominant-baseline (nth 3 Y)
                  )
    (ofm-svg-text g '("no")
                  :x (nth 0 N)
                  :y (nth 1 N)
                  :text-anchor (nth 2 N)
                  :dominant-baseline (nth 3 N)
                  )
    (ofm-debug "draw: seul = %s" (slot-value item 'seul))
    (when (slot-value item 'seul)
      (ofm-debug "draw seul")
      (let* ((D1 20)
             (D2 10)
             (s (cond ((or (string= "TB" dir) (string= "BT" dir)) (ofm-a item 'east))
                      ((or (string= "LR" dir) (string= "RL" dir)) (ofm-a item 'south))
                      )
                )
             (e (cond ((string= "TB" dir) (ofm-a item 'north))
                      ((string= "BT" dir) (ofm-a item 'south))
                      ((string= "LR" dir) (ofm-a item 'west))
                      ((string= "RL" dir) (ofm-a item 'east))
                      )
                )
             (s2 (ofm-coord-plus s
                                 (cond ((or (string= "TB" dir) (string= "BT" dir)) (ofm-coords :x D1 :y 0 ))
                                       ((or (string= "LR" dir) (string= "RL" dir)) (ofm-coords :x 0  :y D1))
                                       )
                                 1 t)
                 )
             (s3 (ofm-coord-plus s2
                                 (cond ((string= "TB" dir) (ofm-coords :x 0 :y (- (ofm-coord-dy s e) D2)))
                                       ((string= "BT" dir) (ofm-coords :x 0 :y (+ (ofm-coord-dy s e) D2)))
                                       ((string= "LR" dir) (ofm-coords :x (- (ofm-coord-dx s e) D2) :y 0))
                                       ((string= "RL" dir) (ofm-coords :x (+ (ofm-coord-dx s e) D2) :y 0))
                                       ) 
                                 1 t)
                 )
             (s4 (ofm-coord-plus s3
                                 (cond ((or (string= "TB" dir) (string= "BT" dir))
                                        (ofm-coords :x (ofm-coord-dx s3 e) :y 0 ))
                                       ((or (string= "LR" dir) (string= "RL" dir))
                                        (ofm-coords :x 0  :y (ofm-coord-dy s3 e)))
                                       )
                                 1 t)
                 )
             )
        (ofm-arrow s e g `(,s2 ,s3 ,s4))
        )
      )
    (ofm-draw-connection item canvas)
    )
  ;; DEBUG
  (ofm-drawInOut item canvas)
  ;; END DEBUG
  )
;;|--------------------------------------------------------------
;;|Description : draw the decision shade
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 27-14-2023 18:14:28
;;|--------------------------------------------------------------
(cl-defmethod ofm-item-draw-shade ((item ofm-decision) canvas)
  (if ofm-shadows
      (let* ((w (slot-value item 'w))
             (h (slot-value item 'h))
             (xc (ofm-x item 'center))
             (yc (ofm-y item 'center))
             (sw 4)
             (sd 5)
             (d 1)
             (sw2 (* sw 0.7071067812)) ;;sin(45)
             (sw3 (* sw 0.3826834324)) ;;sin(22.5)
             (x1 (+ (- xc (* 0.5 (- w h))) sd))
             (x6 (+ xc (* 0.5 (- w h))))
             (x5 (+ xc (- (* 0.5 w) d)))
             (x2 (+ x1 sw))
             (x3 (+ x6 sw3))
             (x4 (+ x5 sw2))
             (y1 (+ yc (* 0.5 h)))
             (y6 y1)
             (y5 (+ yc d))
             (y2 (+ y1 sw))
             (y3 y2)
             (y4 (+ y5 sw2))
             )
        (svg-polyline canvas `((,x1 . ,y1)
                               (,x2 . ,y2)
                               (,x3 . ,y3)
                               (,x4 . ,y4)
                               (,x5 . ,y5)
                               (,x6 . ,y6)
                               (,x1 . ,y1))
                      :stroke "none"
                      :fill "#4e4e4e"
                      )
        )
    )
  )
