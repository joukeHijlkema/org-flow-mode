;;|--------------------------------------------------------------
;;|Description : An until loop
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 29-46-2023 11:46:24
;;|--------------------------------------------------------------
(push '("until" . ("DIR" "FROM" "FROM_PORT" "SEUL" "INVERSE")) ofm-item-inputs)
(defclass ofm-until (ofm-group)
  ((dec :initarg :dec)
   (start :initarg :start)
   )
  "An until loop"
  )
(cl-defmethod initialize-instance :after ((item ofm-until) &rest args)
  (ofm-debug  "until constructor")
  (set-slot-value item 'type "until")
  (let* ((id (org-id-new))
         (First (ofm-item-from-id (nth 0 (slot-value item 'items))))
         (Last (ofm-item-from-id (car (last (slot-value item 'items)))))
         (dec (ofm-decision :text (slot-value item 'text)
                            :id id
                            :bg (or (org-entry-get (point) "BACKGROUND") "green")
                            :dir (if Last (slot-value Last 'dir) nil)
                            :seul nil
                            :from (if Last (slot-value Last 'id) nil)
                            )
              )
         )
    (set-slot-value item 'start First)
    (ofm-addItem item dec t)
    (set-slot-value item 'dec dec)
    (ofm-create-anchors item)
    )
  )
;;|--------------------------------------------------------------
;;|Description : Draw the until
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 06-22-2023 15:22:13
;;|--------------------------------------------------------------
(cl-defmethod ofm-draw-item ((item ofm-until) canvas)
  (ofm-info item "draw until")
  (let* ((g (svg-node canvas 'g))
         (dec (slot-value item 'dec) )
         (from (ofm-from dec))
         (dir (slot-value from 'dir))
         (start (slot-value item 'start))
         (s (cond ((string= "TB" dir) (ofm-a dec 'east))
                  ((string= "BT" dir) (ofm-a dec 'east))
                  ((string= "LR" dir) (ofm-a dec 'south))
                  ((string= "RL" dir) (ofm-a dec 'south))
                  )
            )
         (e (cond ((string= "TB" dir) (ofm-a start 'east))
                  ((string= "BT" dir) (ofm-a start 'east))
                  ((string= "LR" dir) (ofm-a start 'south))
                  ((string= "RL" dir) (ofm-a start 'south))
                  )
            )
         (dx (+ 10 (* 0.5 (- (max (slot-value dec 'w) (slot-value start 'w)) (slot-value dec 'w)))))
         (dy (+ 10 (* 0.5 (- (max (slot-value dec 'h) (slot-value start 'h)) (slot-value dec 'h)))))
         (d1 (cond ((string= "TB" dir) (ofm-coords :x dx :y 0))
                   ((string= "BT" dir) (ofm-coords :x dx :y 0))
                   ((string= "LR" dir) (ofm-coords :x 0 :y (- dy)))
                   ((string= "RL" dir) (ofm-coords :x 0 :y (- dy)))
                   )
             )
         (d2 (cond ((string= "TB" dir) (ofm-coords :x 0 :y (ofm-coord-dy s e)))
                   ((string= "BT" dir) (ofm-coords :x 0 :y (ofm-coord-dy s e)))
                   ((string= "LR" dir) (ofm-coords :x (ofm-coord-dx s e) :y 0))
                   ((string= "LR" dir) (ofm-coords :x (ofm-coord-dx s e) :y 0))
                   )
             )
         (s2 (cond ((string= "TB" dir) (ofm-coord-plus s d1 1 t))
                   ((string= "BT" dir) (ofm-coord-plus s d1 1 t))
                   ((string= "LR" dir) (ofm-coord-min s d1 1 t))
                   ((string= "RL" dir) (ofm-coord-min s d1 1 t))
                   )
             )
         (s3 (ofm-coord-plus s2 d2 1 t))
         )
    (cl-loop for i in (slot-value item 'items) do (ofm-draw-item (ofm-item-from-id i) g))
    (ofm-arrow s e g `(,s2 ,s3))
    )
  )
