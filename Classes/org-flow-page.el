;;|--------------------------------------------------------------
;;|Description : A page class
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-43-2023 09:43:45
;;|--------------------------------------------------------------
(defclass ofm-page ()
  ((size :initarg :size
         :initform "A4")
   (orientation :initarg :orientation
                :initform "Portrait")
   (w :initarg :w
      :initform 0)
   (h :initarg :h
      :initform 0)
   (items :initarg :items
          :initform '()
          :type list)
   (n :initargs :n
      :initform 0)
   (path :initargs :path
         :initforms "")
   )
  "A page class"
  )
;;|--------------------------------------------------------------
;;|Description : initialise the page
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-51-2023 09:51:05
;;|--------------------------------------------------------------
(cl-defmethod initialize-instance ((page ofm-page) &rest args)
  (ofm-debug "page constructor args = %s" args)
  (let* ((s (plist-get (car args) :size))
         (o (plist-get (car args) :orientation))
         (w (nth 0 (plist-get ofm-page-dimensions (intern (format "%s_%s" s o)))))
         (h (nth 1 (plist-get ofm-page-dimensions (intern (format "%s_%s" s o)))))
         )
    (set-slot-value page 'w w)
    (set-slot-value page 'h h)
    (if (plist-member (car args) :n) (set-slot-value page 'n (plist-get (car args) :n)))
    (if (plist-member (car args) :path) (set-slot-value page 'path (plist-get (car args) :path)))
    )
  )
;;|--------------------------------------------------------------
;;|Description : Draw the page
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-02-2023 10:02:17
;;|--------------------------------------------------------------
(cl-defmethod ofm-page-draw ((page ofm-page) &rest args)
  (ofm-debug "draw page %s" (slot-value page 'n))
  (let* ((canvas (svg-create (slot-value page 'w) (slot-value page 'h)
                             :viewBox (format "0 0 %s %s" (slot-value page 'w) (slot-value page 'h))
                             :stroke-width 1
                             :stroke "black"
                             :font-family ofm-font-name
                             :font-size ofm-font-size
                             :alignment-baseline "middle"
                             :dominant-baseline "middle"
                             )
                 )
         )
    (ofm-svg-pageNumber canvas (slot-value page 'n) (slot-value page 'w))
    (if ofm-page-frame (ofm-svg-frame canvas (slot-value page 'w) (slot-value page 'h)))
    (cl-loop for item in (slot-value page 'items) do
             (ofm-draw-item item canvas)
             )
    (with-temp-file (slot-value page 'path)
      (set-buffer-multibyte nil)
      (insert "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"no\"?>")
      (svg-print canvas)
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : get the las item on the page
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 23-18-2023 14:18:37
;;|--------------------------------------------------------------
(cl-defmethod ofm-page-last ((page ofm-page) )
  (ofm-last (car (last (slot-value page 'items))))
  )
;;|--------------------------------------------------------------
;;|Description : get the las item on the page
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 23-18-2023 14:18:37
;;|--------------------------------------------------------------
(cl-defmethod ofm-page-first ((page ofm-page))
  (ofm-first (nth 0 (slot-value page 'items)))
  )
;;|--------------------------------------------------------------
;;|Description : position elements on the page
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 24-46-2023 11:46:18
;;|--------------------------------------------------------------
(cl-defmethod ofm-page-position ((page ofm-page) )
  (let* ((BB (ofm-list-BB (slot-value page 'items)))
         (offset (ofm-coords
                  :x (- ofm-group-margin (nth 0 BB))
                  :y (- (+ 20 ofm-group-margin) (nth 1 BB))
                  )
                 )
         )
    (cl-loop for i in (slot-value page 'items) do (ofm-translate-item i offset))
    )
  )
