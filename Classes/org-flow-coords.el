;;|--------------------------------------------------------------
;;|Description : coordinates and operations
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 27-44-2023 13:44:43
;;|--------------------------------------------------------------
(defclass ofm-coords ()
  ((x :initarg :x)
   (y :initarg :y))
  "coordinates and operations"
  )
;;|--------------------------------------------------------------
;;|Description : draw an arrow between 2 points
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 28-46-2023 09:46:41
;;|--------------------------------------------------------------
(cl-defmethod ofm-arrow ((c1 ofm-coords) c2 g &optional via text)
  (let* ((P (ofm-line c1 c2 g via text))
         (c1 (ofm-coords :x (car (nth (- (length P) 2) P))
                         :y (cdr (nth (- (length P) 2) P))))
         (f (- 1 (/ 3 (ofm-coord-dist c1 c2))))
         (c3 (ofm-coord-plus c1 (ofm-coord-min c2 c1 f t) 1 t))
         (c4 (ofm-coord-angle c2 c3 c2 0.3 3.0))
         (c5 (ofm-coord-angle c2 c3 c2 -0.3 3.0))
         )
    (ofm-debug "\n==== arrow ====\nf=%s\nc1=%s\nc2=%s\nc3=%s\nc4=%s\nc5=%s" f c1 c2 c3 c4 c5)
    (ofm-debug "c2-c1 = %s" (ofm-coord-min c2 c1 f t))
      
    (svg-polyline g `((,(slot-value c3 'x).,(slot-value c3 'y))
                      (,(slot-value c4 'x).,(slot-value c4 'y))
                      (,(slot-value c2 'x).,(slot-value c2 'y))
                      (,(slot-value c5 'x).,(slot-value c5 'y))
                      (,(slot-value c3 'x).,(slot-value c3 'y))
                      )
                  :stroke "black"
                  :fill "black")
    (if text (svg-text g text
                       :x (slot-value c1 'x)
                       :y (slot-value c1 'y)
                       )
      )
    )
  (ofm-debug "==== END arrow ====")
  )
;;|--------------------------------------------------------------
;;|Description : draw a connection
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 15-40-2024 15:40:13
;;|--------------------------------------------------------------
(cl-defmethod ofm-line ((c1 ofm-coords) c2 g &optional via text)
   (let* ((P `((,(slot-value c2 'x) . ,(slot-value c2 'y))))
         )
    (cl-loop for v in (reverse via) do (push `(,(slot-value v 'x) . ,(slot-value v 'y)) P))
    (push `(,(slot-value c1 'x) . ,(slot-value c1 'y)) P)
    (svg-polyline g P
                  :stroke "black"
                  :fill "none")
    P
    )
   )
;;|--------------------------------------------------------------
;;|Description : return the end point of a vector constructed by
;;| rotating f*(c2-c1) over alpha degrees around c3
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 28-47-2023 09:47:04
;;|--------------------------------------------------------------
(cl-defmethod ofm-coord-angle ((c1 ofm-coords) c2 c3 alpha f)
  (let* ((x (* f (- (slot-value c2 'x) (slot-value c1 'x))))
         (y (* f (- (slot-value c2 'y) (slot-value c1 'y))))
         )
    (ofm-coords :x (+ (slot-value c3 'x) (- (* x (cos alpha)) (* y (sin alpha))))
                :y (+ (slot-value c3 'y) (+ (* x (sin alpha)) (* y (cos alpha))))
                )
    )
  )
;;|--------------------------------------------------------------
;;|Description : add 2 points
;;|NOTE : either create a new one or change c1
;;|-
;;|Author : jouke hylkema
;;|date   : 28-48-2023 09:48:35
;;|--------------------------------------------------------------
(cl-defmethod ofm-coord-plus ((c1 ofm-coords) c2 f &optional new)
  (if new
      (ofm-coords :x (+ (slot-value c1 'x) (* f (slot-value c2 'x)))
                  :y (+ (slot-value c1 'y) (* f (slot-value c2 'y))))
    (set-slot-value c1 'x (+ (slot-value c1 'x) (* f (slot-value c2 'x))))
    (set-slot-value c1 'y (+ (slot-value c1 'y) (* f(slot-value c2 'y))))
    )
  )
;;|--------------------------------------------------------------
;;|Description : substract 2 points
;;|NOTE : either create a new one or change c1
;;|-
;;|Author : jouke hylkema
;;|date   : 28-48-2023 09:48:35
;;|--------------------------------------------------------------
(cl-defmethod ofm-coord-min ((c1 ofm-coords) c2 f &optional new)
  (if new
      (ofm-coords :x (* f (- (slot-value c1 'x) (slot-value c2 'x)))
                  :y (* f (- (slot-value c1 'y) (slot-value c2 'y))))
    (set-slot-value c1 'x (* f (- (slot-value c1 'x) (slot-value c2 'x))))
    (set-slot-value c1 'y (* f (- (slot-value c1 'y) (slot-value c2 'y))))
    )
  )
;;|--------------------------------------------------------------
;;|Description : the vector spanning from c1 to c2 
;;|NOTE : either create a new one or change c1
;;|-
;;|Author : jouke hylkema
;;|date   : 28-49-2023 09:49:21
;;|--------------------------------------------------------------
(cl-defmethod ofm-coord-diff ((c1 ofm-coords) c2 f &optional new)
  (if new
      (ofm-coords :x (+ (slot-value c1 'x) (* f (- (slot-value c2 'x) (slot-value c1 'x))))
                  :y (+ (slot-value c1 'y) (* f (- (slot-value c2 'y) (slot-value c1 'y)))))
    (set-slot-value c1 'x (+ (slot-value c1 'x) (* f (- (slot-value c1 'x) (slot-value c2 'x)))))
    (set-slot-value c1 'y (+ (slot-value c1 'y) (* f (- (slot-value c1 'y) (slot-value c2 'y)))))
    )
  )
;;|--------------------------------------------------------------
;;|Description : the horizontal component from c2-c1
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 15-25-2024 15:25:05
;;|--------------------------------------------------------------
(cl-defmethod ofm-coord-min-x ((c1 ofm-coords) c2)
  (let ((tmp (ofm-coord-min c1 c2 1.0 t)))
    (ofm-coords :x (slot-value tmp 'x) :y 0)
    )
  )
;;|--------------------------------------------------------------
;;|Description : the vertical component from c2-c1
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 15-27-2024 15:27:35
;;|--------------------------------------------------------------
(cl-defmethod ofm-coord-min-y ((c1 ofm-coords) c2)
   (let ((tmp (ofm-coord-min c1 c2 1.0 t)))
    (ofm-coords :x 0 :y (slot-value tmp 'y))
    )
  )
;;|--------------------------------------------------------------
;;|Description : get the difference in y for two coordinates
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 31-33-2023 17:33:01
;;|--------------------------------------------------------------
(cl-defmethod ofm-coord-dx ((c1 ofm-coords) c2)
  (- (slot-value c2 'x) (slot-value c1 'x))
  )
;;|--------------------------------------------------------------
;;|Description : get the difference in x for two coordinates
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 31-33-2023 17:33:01
;;|--------------------------------------------------------------
(cl-defmethod ofm-coord-dy ((c1 ofm-coords) c2)
  (- (slot-value c2 'y) (slot-value c1 'y))
  )
;;|--------------------------------------------------------------
;;|Description : the norm of a vector spanned by c1 and c2
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 31-50-2023 18:50:18
;;|--------------------------------------------------------------
(cl-defmethod ofm-coord-dist ((c1 ofm-coords) c2)
  (sqrt (+ (expt (- (slot-value c2 'x) (slot-value c1 'x)) 2)
           (expt (- (slot-value c2 'y) (slot-value c1 'y)) 2)
           )
        )
  )
;;|--------------------------------------------------------------
;;|Description : return the coordinates as an alist
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 25-51-2023 09:51:16
;;|--------------------------------------------------------------
(cl-defmethod ofm-coord-alist ((item ofm-coords) )
  `((,(slot-value item 'x) . ,(slot-value item 'y)))
  )
;;|--------------------------------------------------------------
;;|Description : return coords for an svg ellips
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 31-04-2023 15:04:30
;;|--------------------------------------------------------------
(cl-defmethod ofm-coord-elips ((item ofm-coords) r1 r2 &rest flags)
  `((,r1 ,r2 , (slot-value item 'x)  ,(slot-value item 'y) :sweep ,(plist-get flags :sweep)))
  )
;;|--------------------------------------------------------------
;;|Description : get X or Y value
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 29-12-2024 17:12:40
;;|--------------------------------------------------------------
(cl-defmethod ofm-X ((item ofm-coords) )
  (slot-value item 'x)
  )
(cl-defmethod ofm-Y ((item ofm-coords) )
  (slot-value item 'y)
  )
;;|--------------------------------------------------------------
;;|Description : get BB of a list of points
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 04-42-2024 17:42:06
;;|--------------------------------------------------------------
(defun ofm-coords-BB (points)
  "get BB of a list of points"
  (let ((Xmax (cl-loop for p in points maximize (ofm-X p)))
        (Ymax (cl-loop for p in points maximize (ofm-Y p)))
        (Xmin (cl-loop for p in points minimize (ofm-X p)))
        (Ymin (cl-loop for p in points minimize (ofm-Y p)))
        )
    (list Xmin Ymin (- Xmax Xmin) (- Ymax Ymin))
    )
  )
;;|--------------------------------------------------------------
;;|Description : prety print coord
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 09-32-2024 13:32:55
;;|--------------------------------------------------------------
(cl-defmethod ofm-coords-print ((item ofm-coords))
  (format "%s , %s" (slot-value item 'x) (slot-value item 'y))
  )
(provide 'org-flow-coords)
