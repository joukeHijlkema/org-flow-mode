;;|--------------------------------------------------------------
;;|Description : a delay
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 25-40-2023 09:40:11
;;|--------------------------------------------------------------
(push '("delay" . ("DIR" "FROM" "FROM_PORT")) ofm-item-inputs)

(defclass ofm-delay (ofm-item)
  ()
  "a delay"
  )
;;|--------------------------------------------------------------
;;|Description : constructor
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 25-40-2023 09:40:33
;;|--------------------------------------------------------------
(cl-defmethod initialize-instance :after ((item ofm-delay) &rest args)
  (ofm-debug "delay constructor args=%s" args)
  (set-slot-value item 'type "delay")
  ;; we have to adapt the width because of the shape
  (set-slot-value item 'w (+ (slot-value item 'w) (slot-value item 'h)))
  (ofm-create-anchors item)
  )
;;|--------------------------------------------------------------
;;|Description : info
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 25-41-2023 09:41:48
;;|--------------------------------------------------------------
(cl-defmethod ofm-info ((item ofm-delay) &optional text force)
  (let ((slots (list 'id
                     'type
                     'in
                     'out
                     'anchors
                     'w
                     'h
                     'text
                     'from
                     'dir
                     'bg
                     'align
                     )
               )
        )
    (cl-call-next-method item text slots force)
    )
  )
;;|--------------------------------------------------------------
;;|Description : draw
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 25-42-2023 09:42:22
;;|--------------------------------------------------------------
(cl-defmethod ofm-draw-item ((item ofm-delay) canvas)
  (ofm-info item "Draw delay ")
  (let* ((g (svg-node canvas 'g))
         (x (ofm-x item))
         (y (ofm-y item))
         (w (slot-value item 'w))
         (h (slot-value item 'h))
         (xc (+ x (* 0.5 w)))
         (nl (length (slot-value item 'lines)))
         (D  (/ h nl))
         (yc (+ y (* 0.5 D)))
         (d1 (ofm-coords :x (- w (* 0.5 h)) :y 0))
         (d2 (ofm-coords :x 0 :y h))
         (p1 (ofm-coords :x x :y y))
         (p2 (ofm-coord-plus p1 d1 1 t))
         (p3 (ofm-coord-plus p2 d2 0.5 t))
         (p4 (ofm-coord-plus p2 d2 1 t))
         (p5 (ofm-coord-plus p1 d2 1 t))
         (r (* 0.5 h))
         )
    (ofm-item-draw-shade item g)
    (svg-path g `((moveto ,(ofm-coord-alist p1))
                  (lineto ,(ofm-coord-alist p2))
                  (elliptical-arc ,(ofm-coord-elips p4 r r :sweep t))
                  (lineto ,(ofm-coord-alist p5))
                  (closepath)
                  )
              :fill-opacity 0.5
              :fill (slot-value item 'bg)
              )
    (ofm-svg-text g (slot-value item 'lines)
                  :x xc
                  :y yc
                  :text-anchor "middle"
                  :text-align (slot-value item 'align)
                  :alignment-baseline "middle"
                  :fill "black"
                  :stroke "none"
                  )
    (ofm-draw-connection item canvas)
    )
  (ofm-drawInOut item canvas)
  )
;;|--------------------------------------------------------------
;;|Description : draw a shade
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 31-46-2023 09:46:07
;;|--------------------------------------------------------------
(cl-defmethod ofm-item-draw-shade ((item ofm-delay) canvas)
  (if ofm-shadows
      (let* ((x (ofm-x item))
             (y (ofm-y item))
             (w (slot-value item 'w))
             (h (slot-value item 'h))
             (sw 4)
             (sd 5)
             (r (* 0.5 h))
             (w2 (- w r))
             (h2 (* 0.5 h))
             (alpha (- 0.7853981634 (asin (/ (* 1.414213562 sw) (* 2 r)))))
             (d0 (ofm-coords :x sd
                             :y 0))
             (d1 (ofm-coords :x (- w2 sd)
                             :y 0))
             (d2 (ofm-coords :x sw
                             :y sw))
             (d3 (ofm-coords :x (* r (cos alpha))
                             :y (- (* r (sin alpha)))))
             (d4 (ofm-coords :x 0
                             :y (- h2)))
             (d5 (ofm-coords :x 0
                             :y sw))
             (p0 (ofm-coords :x x
                             :y (+ y h)))
             (p1 (ofm-coord-plus p0 d0 1 t))
             (p2 (ofm-coord-plus p1 d1 1 t))
             (p3 (ofm-coord-plus p2 d4 1 t))
             (p4 (ofm-coord-plus p3 d3 1 t))
             (p5 (ofm-coord-plus p2 d2 1 t))
             (p6 (ofm-coord-plus p1 d5 1 t))
             )
        (svg-path g `((moveto ,(ofm-coord-alist p1))
                      (lineto ,(ofm-coord-alist p2))
                      (elliptical-arc ,(ofm-coord-elips p4 r r))
                      (elliptical-arc ,(ofm-coord-elips p5 r r :sweep t))
                      (lineto ,(ofm-coord-alist p6))
                      (closepath)
                      )
                  :stroke "none"
                  :fill "#4e4e4e"
                  )
        )
    )
  )
