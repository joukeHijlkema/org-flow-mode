# org-flow

Work in progress

## Description
Unhappy with all the flowchart-as-code programs that I came across I decided to build my own based on a org-mode subtree. The example chart (Example/test.org "The chart") gives the following :

![Page 1](./Example/flowchart_0-crop.svg "first page of the flowchart")

Note that the final graph is split into pages

![Page 2](./Example/flowchart_1-crop.svg "second page of the flowchart")

You can also have your graph split per group for easy inclusion in documents

![Page 3](./Example/flowchart_This_is_a_group-crop.svg "An element of the flowchart")

## Installation
I use straight for my installation. Vanila use-package should work more or les the same.

```(use-package org-flow-mode
  :straight (:type git
             :host gitlab
             :repo "joukeHijlkema/org-flow-mode"
             :files ("*")
             )
)
```

## Usage
There is a (rudimentary) user interface (see org-flow-ui.el).
M-x ofm-insert-flowchart will insert a header like this
```* The chart
:PROPERTIES:
:FORMAT:   svg
:PATH: /tmp/flowchart
:FLOWCHART: start
:TEXT_MARGIN: 10
:GROUP_MARGIN: 8
:FONT_SIZE: 10
:ARROW_LENGTH: 28
:FONT_NAME: DejaVu Sans
:ID:       2deb7c0d-e572-4eb8-b9af-972b71cd979f12
:VERBOSE:  0
:PAGE_SIZE: A4
:PAGE_ORIENTATION: Portrait
:DRAW_GROUPS: nil
:SHADOWS:  t
:FRAME:    nil
:END:
```

where the different properties are
  * :FORMAT: This is svg only for the moment
  * :PATH: The base name of the flowchart images (/tmp/flowchart_#.svg for pages, /tmp/flowchart_name.svg for groups)
  * :FLOWCHART: Needed to detect the beginning of the chart
  * :TEXT_MARGIN: A margin, play with it to see what it does
  * :GROUP_MARGIN: Another margin
  * :FONT_SIZE: The size of the fonts
  * :ARROW_LENGTH: The minimum length of the arrows
  * :FONT_NAME: The font type
  * :ID: A unique id needed to identify the elements. Don't touch this unless you know what you're doing.
  * :VERBOSE: For debugging
  * :PAGE_SIZE: The page size. Only A4 for the moment. It's easy to add formats to the variable ofm-page-dimensions in org-flow-mode.el
  * :PAGE_ORIENTATION: See :PAGE-SIZE:
  * :DRAW_GROUPS: Say t if you want an individual image per group, nil otherwise
  * :SHADOWS: Say t if you want shadows, nil otherwise
  * :FRAME: Say t if you want a frame around the page, nil otherwise
  
after that you can add different new items by calling M-x ofm-add-item while inside the chart subtree. If you want to insert and item between two existing items use M-x ofm-insert-item while inside the first of the two existing items. The text that follows the header will be used as text for the item.

For all items (not groups), you use the :FROM: property to define the hierarchy. The value of :FROM: is the :ID: of the item the precedes the actual item (there will be an arrow from the item with :ID: = :FROM: to the actual item)

To debug your chart, all items have a :DISABLE: property that, when set to t, disables the item (and its children if relevant)

If you don't want to use the UI, you can add items by creating a header of the form:
 `* item_name <some text>`

### The different item types

#### rect
This is a simple rectangle with text. It allows for the following properties:

    * :BACKGROUND: The background color
    * :DIR: The direction
      * TB = Top to bottom
      * BT = Bottom to top
      * LR = Left to right
      * RL = Right to left
      
#### check
Is like rect but with an extra check box in front of the text

#### decision
This is a decision box. It has the same properties as Rect.

#### delay
This is a delay box. It has the same properties as Rect.
    
#### list
A list box. Every line of text will get a bullet point. It has the same properties as Rect.

#### nlist
A numbered list box. Every line of text will get a number. It has the same properties as Rect.

#### Group
This is a group box. Its purpose is to group several items together. The inner text is used as its title.

#### Until
The most complex of items (sofar). It is a group that represents an until loop. Repeat all the steps until the condition given as the inner text of the header is true. (See the example chart to get an idea)

## Support
I'm rather busy but you can always try to drop me a line if you have problems, need help or have an idea for improvement.

## Contributing
Please feel free to modify the code as you see fit. If you create something nice, let me know and I'll try it include it.

## License
No lincense

## Project status
Working on this when I can/want/need to.
