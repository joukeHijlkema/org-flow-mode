#!/bin/bash

for f in $(ls *.svg)
do
    base="${f%.*}"
    echo "$f -> $base.pdf"
    inkscape --export-type="svg" --export-area-drawing -o $base-crop.svg $f 
    inkscape --export-type="pdf" -o $base.pdf $f
    pdfcrop $base.pdf
    # convert $f $base.pdf
done
